<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CdController extends Controller
{
    /**
     * @Route("/cd", name="Cd")
     */
    public function consultAction(Request $request, $id)
    {
        $repositoryCd = $this->getDoctrine()->getManager()->getRepository("AppBundle:Cd");

        $cd = $repositoryCd->find($id);

        return $this->render('cd/consult.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'cd' => $cd
        ]);
    }

    /**
     * @Route("/cd/borrow", name="Cd_borrow")
     */
    public function borrowAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('cd/validation.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    public function getUploadDir(Request $request){
        return $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath();
    }

}
