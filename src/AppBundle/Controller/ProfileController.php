<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cd;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


class ProfileController extends Controller
{
    /**
     * @Route("/profile", name="Profile")
     */
    public function consultAction(Request $request, $id)
    {

        $repositoryUser= $this->getDoctrine()->getManager()->getRepository("AppBundle:User");

        $repositoryCd = $this->getDoctrine()->getManager()->getRepository("AppBundle:Cd");

        $user = $repositoryUser->find($id);

        $shared = $repositoryCd->findByOwner($id);

        $borrowed = [];

        // replace this example code with whatever you need
        return $this->render('profile/consult.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $user,
            'shared' => $shared,
            'borrowed' => $borrowed,
        ]);
    }

    /**
     * @Route("/profile/edit", name="Edit")
     */
    public function editAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('profile/edit.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/profile/share", name="Share")
     */
    public function shareAction(Request $request, $id)
    {
        $repositoryUser= $this->getDoctrine()->getManager()->getRepository("AppBundle:User");
        $repositoryCd = $this->getDoctrine()->getManager()->getRepository("AppBundle:Cd");

        $user = $repositoryUser->find($id);

        $cd = new Cd();

        $formBuilder = $this->createFormBuilder($cd);

        $formBuilder->add('title', TextType::class)
            ->add('author',TextType::class)
            ->add('description', TextareaType::class)
            ->add('image',FileType::class)
            ->add('save', SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em=$this->getDoctrine()->getManager();

            $cd->setOwner($repositoryUser->findOneById($user->getId()));

            /*$path = $this->getUploadDir($request);

            $cd->setPath($path);*/

            $em->persist($cd);
            $em->flush();

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('profile/share.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'form' => $form->createView(),
            'user' => $user,
        ]);

        return $this->render('profile/consult.html.twig', array('user' => $id));
    }

    public function getUploadDir(Request $request){
        return $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath();
    }
}
