<?php

namespace controller;

class base extends AbstractController
{

    public function setDynamicData($local, $params = null)
    {
        $local = parent::setDynamicData($local, $params);

        //Call api here and return $local ( if needed )

        return $local;
    }

}