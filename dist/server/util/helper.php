<?php

function countFiles($dir){
    $i = 0;
    if ($handle = opendir($dir)) {
        while (($file = readdir($handle)) !== false){
            if (!in_array($file, array('.', '..')) && !is_dir($dir.$file))
            $i++;
        }
    }
    // prints out how many were in the directory
    return $i;
}

function stripArray($tab){
    $tab = str_replace("[","",$tab);
    $tab = str_replace("]","",$tab);
    $tab = str_replace("\"","",$tab);
    $tab = str_replace("'","",$tab);
    $tab = explode(",",$tab);
    return $tab;
}

function getAlias($label){
    $label = str_replace(" ","-",strtolower(normalizeChars($label)));
    $label = str_replace("'","-",$label);
    return $label;
}

function normalizeChars($s) {
    $replace = array(
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'Ae', 'Å'=>'A', 'Æ'=>'A', 'Ă'=>'A', 'Ą' => 'A', 'ą' => 'a',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'ae', 'å'=>'a', 'ă'=>'a', 'æ'=>'ae',
        'þ'=>'b', 'Þ'=>'B',
        'Ç'=>'C', 'ç'=>'c', 'Ć' => 'C', 'ć' => 'c',
        'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ę' => 'E', 'ę' => 'e',
        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e',
        'Ğ'=>'G', 'ğ'=>'g',
        'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'İ'=>'I', 'ı'=>'i', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i',
        'Ł' => 'L', 'ł' => 'l',
        'Ñ'=>'N', 'Ń' => 'N', 'ń' => 'n',
        'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe', 'Ø'=>'O', 'ö'=>'oe', 'ø'=>'o',
        'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
        'Š'=>'S', 'š'=>'s', 'Ş'=>'S', 'ș'=>'s', 'Ș'=>'S', 'ş'=>'s', 'ß'=>'ss', 'Ś' => 'S', 'ś' => 's',
        'ț'=>'t', 'Ț'=>'T',
        'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'Ue',
        'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'ue',
        'Ý'=>'Y',
        'ý'=>'y', 'ý'=>'y', 'ÿ'=>'y',
        'Ž'=>'Z', 'ž'=>'z', 'Ż' => 'Z', 'ż' => 'z', 'Ź' => 'Z', 'ź' => 'z'
    );
    $specials = "%!:/;.,*\$£¤}=])@^_\\`|[({'`#\"~&+-°<>²";
    $specials = str_split($specials);
    foreach($specials as $special){
        $replace[$special] = "";
    }
    return strtr($s, $replace);
}

function truncate($string,$maxchar=20,$postfixe="..."){
    $count = strlen($string);
    $truncate = $string;
    if($count > $maxchar){
        $truncate = substr_replace($string,$postfixe, $maxchar,$count);
    }
    return $truncate;
}

function decomposeDate($date){
    $time = strtotime($date);
    $rs = new stdClass();
    $rs->day = date("d",$time);
    $rs->month = date("m",$time);
    $rs->year = date("Y",$time);
    return $rs;
}

function obfuscate($email){
    $length = strlen($email);
    $obfuscated = "";
    for ($i = 0; $i < $length; $i++){
        $obfuscated .= "&#" . ord($email[$i]).";";
    }
    return $obfuscated;
}