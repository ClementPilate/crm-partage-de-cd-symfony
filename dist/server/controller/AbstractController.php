<?php
namespace controller;

class AbstractController
{

    protected $params = null;

    public function __construct()
    {
    }

    /**
     * Return data from apis to add in the page or anything that isn't in the local
     * @param \stdClass $params
     */
    public function getData($params = null)
    {
        $data = new \stdClass();
        $data->params = new \stdClass();

        foreach ($params as $k => $param) {
            $data->params->{$k} = $param;
        }

        $this->params = $data->params;
        return $data;
    }

    public function setDynamicData($localPage)
    {
        return $localPage;
    }
}