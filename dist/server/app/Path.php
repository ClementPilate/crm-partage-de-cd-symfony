<?php
namespace app;

use util\MobileDetect;

class Path
{
    protected static $instance;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    const DEVICE_DESKTOP = "desktop";
    const DEVICE_MOBILE = "mobile";
    const DEVICE_DEFAULT = "default";

    public $url = null;
    public $file = null;
    public $detect = null;
    public $device = "";

    public function set($config)
    {
        $this->setDevice($config);
        $this->setUrl($config);
        $this->setPath();
    }

    private function setDevice($config)
    {
        //Detect device type (mobile or desktop)DEVICE_DESKTOP
        $this->device = $config->devices->list[0];
        if (in_array(Path::DEVICE_MOBILE, $config->devices->list)) {
            $this->detect = new MobileDetect();
            if ($this->detect->isMobile() && !$this->detect->isTablet()) $this->device = Path::DEVICE_MOBILE;
        }
        $this->device = $config->devices->forced ? $config->devices->forced : $this->device;
    }

    private function setUrl($config)
    {
        //Set urls
        $this->url = new \stdClass();
        $this->url->base = $config->env->url->base;
        $this->url->device = $this->url->base . $this->device . "/";
        $this->url->assets = $this->url->device . "assets/";
        $this->url->current = $_SERVER["REQUEST_URI"];
        $this->url->fullCurrent = $this->getFullCurrentUrl();
        $this->url->local = $this->url->device . "local/";
        $this->url->tpl = $this->url->device . "tpl/";
        $this->url->page = $this->url->tpl . "page/";
        $this->url->partial = $this->url->tpl . "partial/";
    }

    private function setPath()
    {
        //File path
        $this->file = new \stdClass();
        $this->file->local = $this->device . "/local/";
        $this->file->tpl = $this->device . "/tpl/";
        $this->file->page = $this->file->tpl . "page/";
        $this->file->partial = $this->file->tpl . "partial/";
    }

    private function getFullCurrentUrl()
    {
        $url = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $url .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $url .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $url;
    }

    public function getControllerPath()
    {
        return $this->file->controller;
    }

    public function getBaseFile()
    {
        return $this->file->base;
    }

    public function getBaseUrl($lang = null)
    {
        $_lang = \app\Lang::getInstance();
        $lang = $lang ? $lang : $_lang->name;
        $lang = count($_lang->list) > 1 ? $lang : "";
        return $this->url->base . $lang . "/";
    }

    public function getCurrentUrl()
    {
        return $this->url->current;
    }

    public function getDeviceUrl()
    {
        return $this->url->device;
    }

    public function getAssetsUrl()
    {
        return $this->url->assets;
    }

    public function getPageUrl($page)
    {
        $main = \Main::getInstance();
        $page = trim($page);
        $_lang = \app\Lang::getInstance();
        $lang = count($_lang->list) > 1 ? $_lang->name."/" : "";
        return $this->url->base . $lang . $main->config->routes->{$page}->local->semantic;
    }

    public function getImg($file)
    {
        return $this->url->asset . "img/" . $file;
    }

    public function getTplPath()
    {
        return $this->file->tpl;
    }

    public function getPagePath()
    {
        return $this->file->page;
    }

    public function getPartialPath()
    {
        return $this->file->partial;
    }

    public function getLocalPath()
    {
        return $this->file->local . Lang::getInstance()->name . "/";
    }

}
