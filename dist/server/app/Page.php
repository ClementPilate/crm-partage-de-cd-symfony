<?php
namespace app;

use controller\AbstractController;

class Page
{
    private $route = "";

    /**
     * @var AbstractController
     */
    public $controller = null;
    public $id = "";
    public $params = null;
    public $local = null;
    public $tpl = null;
    public $render = null;


    public function __construct($id, $route, $params)
    {
        $path = Path::getInstance();

        $this->route = $route;
        $this->id = $id;
        $this->setParams($params);
        $this->setController();
        $this->setLocal($path);
        $this->setTpl($path);
        //print_r($this->local);exit;
        //Render tpl
        $this->render = \Main::getInstance()->moustache->render($this->tpl, $this->local);
    }

    private function setParams($params)
    {
        $this->params = new \stdClass();
        if (count($params) > 0 && isset($this->route->params) && count($this->route->params) > 0) {
            foreach ($params as $k => $param) {
                $this->params->{$this->route->params[$k]} = $params[$k];
            }
        }
    }

    private function setController()
    {
        $controllerClass = "controller\\" . ucfirst($this->route->controller->php);
        $this->controller = new $controllerClass($this->params);
    }

    /**
     * @param Path $path
     */
    private function setLocal($path)
    {
        $file = $path->getLocalPath() . "page/" . $this->route->model;
        $this->local = json_decode(file_get_contents($file));
        $this->local->id = $this->id;
        $this->local->data = $this->controller->getData($this->params);
        $this->local->urls = Route::getInstance()->urls;
        $this->local->data = $this->controller->setDynamicData($this->local);
    }

    private function setTpl($path)
    {
        $file = $path->getTplPath() . "page/" . $this->route->tpl;
        $this->tpl = file_get_contents($file);
    }
}