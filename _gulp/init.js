//DECLARE GLOBAL
global.envs = require('../config/env.json');
global.devices = require('../config/devices.json');
global.scripts = {
    js: {
        babel: {},
        concat: {}
    },
    css: [],
    html: [],
    php: [],
    img: {},
    gzip: [],
    svg: []
};


//BUILD SCRIPTS PATHS
var device = "";
var i = 0;
var lg = devices.list.length;
for (i = 0; i < lg; i++) {
    device = devices.list[i];

    //BABEL
    scripts.js.babel[device] = [];
    scripts.js.babel[device].push('dev/' + device + '/js/app/_namespace.js');
    scripts.js.babel[device].push('dev/' + device + '/js/app/_const.js');
    scripts.js.babel[device].push('dev/' + device + '/js/util/*.js');
    scripts.js.babel[device].push('dev/' + device + '/js/manager/*.js');
    scripts.js.babel[device].push('dev/' + device + '/js/partial/*.js');
    scripts.js.babel[device].push('dev/' + device + '/js/controller/Controller.js');
    scripts.js.babel[device].push('dev/' + device + '/js/controller/*.js');
    scripts.js.babel[device].push('dev/' + device + '/js/app/*.js');
    scripts.js.babel[device].push('dev/' + device + '/js/Main.js');

    //CONCAT
    scripts.js.concat[device] = [];
    scripts.js.concat[device].push('dev/' + device + '/js/vendor/*.js');
    scripts.js.concat[device].push('dev/' + device + '/js/script.js');
    scripts.js.concat[device].push('!dev/' + device + '/js/script.min.js');

    //IMG
    scripts.img[device] = [];
    scripts.img[device].push('dev/' + device + '/assets/img/*.{jpg,jpeg,png,webp}');
    scripts.img[device].push('dev/' + device + '/assets/img/**/*.{jpg,jpeg,png,webp}');
    scripts.img[device].push('dev/' + device + '/assets/img/**/**/*.{jpg,jpeg,png,webp}');

    //SCSS
    scripts.css.push('dev/' + device + '/css/**/*.scss');
    scripts.css.push('dev/' + device + '/css/*.scss');

    //HTML TPL
    scripts.html.push('dev/' + device + '/tpl/*.html');
    scripts.html.push('dev/' + device + '/tpl/**/*.html');

    //SVG
    scripts.svg.push('dev/' + device + '/assets/svg/*.svg');

    //PHP
    scripts.php.push('dev/server/*.php');
    scripts.php.push('dev/server/**/*.php');
}

scripts.html.push('dev/index.php');
