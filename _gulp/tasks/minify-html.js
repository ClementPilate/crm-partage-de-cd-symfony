var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');

gulp.task('minify-html', function () {
    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.src(['dev/' + device + '/tpl/*.html', 'dev/' + device + '/tpl/**/*.html'])
            .pipe(htmlmin(
                {
                    collapseWhitespace: true,
                    minifyCSS: true
                }
            ))
            .pipe(gulp.dest('dist/' + device + '/tpl/'));
    }
});