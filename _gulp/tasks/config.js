var gulp = require('gulp');
var jsoncombine = require("../../_gulp/utils/jsoncombine");
var fs = require('fs');
var livereload = require('gulp-livereload');
var gzip = require('gulp-gzip');

gulp.task('config', function () {
    envs = fs.readFileSync('config/env.json', 'utf-8');
    envs = JSON.parse(envs);
    var env = "";
    for (env in envs) {
        var g = gulp.src(
            [
                "config/*.json",
                "config/**/*.json",
                "!config/env.json"
            ]
            )
            .pipe(jsoncombine("config.json", function (data, env) {
                // do any work on data here
                for (s in data) {
                    if (s.indexOf("\\") != -1) {
                        var tabName = s.split("\\");
                        var n = tabName[1];
                        if (!data[tabName[0]])data[tabName[0]] = {};
                        data[tabName[0]][n] = data[s];
                        delete data[s];
                    }
                }
                if (envs[env]["use"]) {
                    var use = envs[env]["use"];
                    data.env = envs[env][use];
                } else {
                    data.env = envs[env];
                }
                data.env.name = env;
                return new Buffer(JSON.stringify(data));
            }, env));
        g.pipe(gulp.dest(env + "/"))
            .pipe(livereload());


        //Build htaccess from env
        var path = env + '/.htaccess';
        fs.writeFileSync(path, fs.readFileSync('base/.htaccess'));
        var data = fs.readFileSync(path, 'utf-8');
        var replacing = "";
        if (envs[env]["use"]) {
            var use = envs[env]["use"];
            replacing = envs[env][use].htaccess.fallbackresource;
        } else {
            replacing = envs[env].htaccess.fallbackresource;
        }


        var newValue = data.replace(/base/g, replacing);
        fs.writeFileSync(path, newValue, 'utf-8');
    }

});