var gulp = require('gulp');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');
var rename = require('gulp-rename');
var gzip = require('gulp-gzip');

gulp.task('concat', function () {

    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.src(scripts.js.concat[device])
            .pipe(concat('script.js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('web/assets/js/'))
            .pipe(livereload());
    }

});