var gulp = require('gulp');
var langs = require('../../config/lang.json');

// Include plugins
var argv = require('yargs').argv;
var mkdirp = require('mkdirp');
var fs = require('fs');
var async = require('async');
var wrench = require('wrench');
var prompt = require('prompt');


gulp.task('page', function () {
    console.log("Création d'une page!".cyan);
    prompt.delimiter = "\n";

    prompt.start();

    prompt.get({
        properties: {
            id: {
                description: "Quel est son identifiant?".blue
            }
        }
    }, function (err, result) {
        if (err) {
            console.log(err)
            return;
        }

        var routePath = "./config/routes/" + result.id + ".json";
        fs.access(routePath, fs.R_OK | fs.W_OK, function (err) {
            if (err) {
                //Build config json from template
                var path = './config/routes/' + result.id + '.json';
                fs.writeFileSync(path, fs.readFileSync('./base/page.json'));
                var data = fs.readFileSync(path, 'utf-8');
                data = data.replace(/base/g, result.id);
                data = JSON.parse(data);
                var i = 0;
                var langsLength = langs.list.length;
                var l = "";
                for (i = 0; i < langsLength; i++) {
                    l = langs.list[i];
                    data.local[l] = {
                        "semantic": result.id,
                        "title": "Page " + l + " " + result.id,
                        "description": "Description " + l + " " + result.id
                    };
                }
                data = JSON.stringify(data);
                fs.writeFileSync(path, data, 'utf-8');

                var lg = devices.list.length;
                var device = "";
                var pathDevice = "";
                var pathImport = "";
                var pathServer = 'dev/server/';
                var controller = "";
                for (i = 0; i < lg; i++) {
                    device = devices.list[i];
                    pathDevice = 'dev/' + device + "/";
                    pathImport = 'dev/' + device + "/css/_import.scss";

                    //Build css file
                    path = pathDevice + "css/page/_" + result.id + ".scss";
                    fs.writeFileSync(path, fs.readFileSync('./base/page.scss'));
                    data = fs.readFileSync(path, 'utf-8');
                    data = data.replace(/base/g, result.id);
                    fs.writeFileSync(path, data, 'utf-8');

                    //Add scss import
                    data = fs.readFileSync(pathImport, 'utf-8');
                    if (data.indexOf("@import 'page/_" + result.id) === -1) {
                        data = data.replace("//end", "@import 'page/" + result.id + "';\n//end");
                        fs.writeFileSync(pathImport, data, 'utf-8');
                    }

                    //Add local json
                    for (var j = 0; j < langsLength; j++) {
                        l = langs.list[j];
                        path = pathDevice + "local/" + l + "/page/" + result.id + ".json";
                        fs.writeFileSync(path, "{}", 'utf-8');
                    }

                    //Add tpl
                    path = pathDevice + "tpl/page/" + result.id + ".html";
                    fs.writeFileSync(path, '<div class="' + result.id + '"></div>', 'utf-8');

                    //Add js
                    controller = result.id.charAt(0).toUpperCase() + result.id.slice(1);
                    path = pathDevice + "js/controller/" + controller + ".js";
                    fs.writeFileSync(path, fs.readFileSync('./base/controller.js'));
                    data = fs.readFileSync(path, 'utf-8');
                    data = data.replace(/base/g, controller);
                    fs.writeFileSync(path, data, 'utf-8');

                    //Add php
                    path = pathServer + "controller/" + controller + ".php";
                    fs.writeFileSync(path, fs.readFileSync('./base/controller.php'));
                    data = fs.readFileSync(path, 'utf-8');
                    data = data.replace(/base/g, controller);
                    fs.writeFileSync(path, data, 'utf-8');
                }

            } else {
                console.log("La page existe déjà".red);
            }
        });
    });

});