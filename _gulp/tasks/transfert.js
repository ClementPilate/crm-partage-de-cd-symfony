var gulp = require('gulp');

gulp.task('transfert', function(){
    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.src('dev/'+device+'/assets/**')
            .pipe(gulp.dest('dist/'+device+'/assets/'));
        gulp.src('dev/'+device+'/local/**')
            .pipe(gulp.dest('dist/'+device+'/local/'));
        gulp.src('dev/server/**')
            .pipe(gulp.dest('dist/server/'));
        gulp.src('dev/index.php')
            .pipe(gulp.dest('dist/'));
    }

});