var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var gzip = require('gulp-gzip');

gulp.task('minify-css', function() {
    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.src('web/assets/css/style.css')
            .pipe(minifyCSS({keepBreaks:true}))
            .pipe(gzip())
            .pipe(gulp.dest('dist/'+device+'/css/'))
    }
});