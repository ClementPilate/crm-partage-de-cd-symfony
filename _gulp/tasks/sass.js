var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var livereload = require('gulp-livereload');
var gzip = require('gulp-gzip');

gulp.task('sass', function () {

    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];

        sass('dev/' + device + '/css/', {compass: true})
            .on('error', sass.logError)
            .pipe(gulp.dest('web/assets/css/'))
            .pipe(livereload());
    }
});