var gulp = require('gulp');
var svgSprite = require('gulp-svg-sprite');
var livereload = require('gulp-livereload');
gulp.task('svgsprite', function () {

    var config = {
        shape: {
            dimension: {                         // Dimension related options
                maxWidth: 1000,                     // Max. shape width
                maxHeight: 1000,                     // Max. shape height
                precision: 2,                        // Floating point precision
                attributes: false                    // Width and height attributes on embedded shapes
            },
            spacing: {                         // Spacing related options
                padding: 0,                        // Padding around all shapes
                box: 'content'                 // Padding strategy (similar to CSS `box-sizing`)
            }
        },
        svg: {
            xmlDeclaration:false,
            doctypeDeclaration:false,
            rootAttributes:{
                style:"position: absolute; width: 0; height: 0;",
                width:"0",
                height:"0"
            }
        },
        mode:{
            symbol:{
                sprite:"svg.html",
                dest:"../partial/"
            }
        }
    };

    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];

        gulp.src(['dev/' + device + '/assets/svg/tosprite/*.svg'])
            .pipe(svgSprite(config))
            .pipe(gulp.dest('app/Resources/partial/'))
            .pipe(livereload());
    }

});