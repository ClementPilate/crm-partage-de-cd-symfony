var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gzip = require('gulp-gzip');

gulp.task('compress', function () {
    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.src('dev/' + device + '/js/script.min.js')
            .pipe(uglify())
            .pipe(gzip())
            .pipe(gulp.dest('dist/' + device + '/js/'))
    }
});