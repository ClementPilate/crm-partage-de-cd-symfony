var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var mozjpeg = require('imagemin-mozjpeg');
var webp = require('gulp-webp');

gulp.task('imagemin', function () {
    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.src(scripts.img[device])
            .pipe(imagemin({
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                optimizationLevel: 3,
                use: [pngquant(), mozjpeg({quality: 60})]
            }))
            .pipe(gulp.dest('dist/' + device + '/assets/img/'));
        //

        gulp.src(scripts.img[device])
            .pipe(webp({quality: 50}))
            .pipe(gulp.dest('dist/' + device + '/assets/img/'));

    }
});

