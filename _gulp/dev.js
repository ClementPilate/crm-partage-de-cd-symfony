var gulp = require('gulp');
var livereload = require('gulp-livereload');

// Rerun the task when a file changes
gulp.task('dev', function () {
    livereload.listen();
    gulp.watch(["config/*.json", "config/**/*.json"], ['config']);

    //console.log(scripts);
    var device = "";
    var i = 0;
    var lg = devices.list.length;
    for (i = 0; i < lg; i++) {
        device = devices.list[i];
        gulp.watch(scripts.js.babel[device], ['babel']);
        gulp.watch(scripts.js.concat[device], ['concat']);
    }

    gulp.watch(scripts.css, ['sass']);
    gulp.watch(scripts.svg, ['svgsprite']);
    gulp.watch(scripts.html).on('change', function (file) {
        livereload.changed(file.path);
    });
    gulp.watch(scripts.php).on('change', function (file) {
        livereload.changed(file.path);
    });
});