"use strict";

var _get = function get(_x9, _x10, _x11) { var _again = true; _function: while (_again) { var object = _x9, property = _x10, receiver = _x11; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x9 = parent; _x10 = property; _x11 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SL = SL || {};
/**
 * @type Main
 */
SL.main = null;
SL.app = {};
SL.controller = {};
SL.manager = {};
SL.util = {};
SL.partial = {};
var DEVICE = {
    mobile: "mobile",
    tablet: "tablet",
    desktop: "desktop",
    "default": "default"
};

var EVENT = {
    resize: "resize",
    click: "click",
    mousemove: "mousemove",
    mouseenter: "mouseenter",
    mouseleave: "mouseleave",
    touchmove: "touchmove",
    focus: "focus",
    blur: "blur",
    scroll: "scroll"
};

var TYPEOF = {
    nodelist: "nodelist",
    element: "element"
};

var SENS_LESS = -1;
var SENS_MORE = 1;
SL.util.Cookie = (function () {
    function Cookie() {
        _classCallCheck(this, Cookie);
    }

    _createClass(Cookie, null, [{
        key: "createCookie",

        /**
         * createCookie
         * @param name
         * @param value
         * @param days
         */
        value: function createCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
                expires = "; expires=" + date.toGMTString();
            }
            var cookie = name + '=' + value + expires + '; path=/';
            document.cookie = cookie;
        }
    }, {
        key: "readCookie",

        /**
         * readCookie
         * @param name
         * @returns {*}
         */
        value: function readCookie(name) {
            var nameEQ = name;
            var ca = document.cookie.split(';');
            var c = 0;
            for (var i = 0; i < ca.length; i++) {
                c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    }, {
        key: "eraseCookie",
        value: function eraseCookie(name) {
            createCookie(name, "", -1);
        }
    }]);

    return Cookie;
})();

SL.util.File = (function () {
    function File() {
        _classCallCheck(this, File);
    }

    _createClass(File, null, [{
        key: "getFile",
        value: function getFile(url, callback, context) {
            var params = arguments.length <= 3 || arguments[3] === undefined ? {} : arguments[3];

            $.ajax({
                type: 'GET',
                url: url,
                data: params,
                timeout: 0,
                context: context,
                success: callback,
                error: function error(xhr, type) {
                    //TODO alert('Ajax error!')
                }
            });
        }
    }, {
        key: "getPage",
        value: function getPage(url, callback, context) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    ajax: true
                },
                timeout: 0,
                context: context,
                success: callback,
                error: function error(xhr, type) {
                    callback();
                }
            });
        }
    }]);

    return File;
})();
SL.util.Object = (function () {
    function Object() {
        _classCallCheck(this, Object);
    }

    _createClass(Object, null, [{
        key: "keyOf",
        value: function keyOf(object, value) {
            var find = false;
            for (var s in object) {
                if (object[s] === value) {
                    find = s;
                    break;
                }
            }
            return find;
        }
    }, {
        key: "clone",
        value: function clone(target) {
            var clone = {};
            for (var s in target) {
                if (target.hasOwnProperty(s)) {
                    clone[s] = target[s];
                }
            }
            return clone;
        }
    }]);

    return Object;
})();
/***************************************************************
 * Helper functions for older browsers
 ***************************************************************/
if (!Object.hasOwnProperty('create')) {
    Object.create = function (parentObj) {
        function tmpObj() {}

        tmpObj.prototype = parentObj;
        return new tmpObj();
    };
}
if (!Object.hasOwnProperty('defineProperties')) {
    Object.defineProperties = function (obj, props) {
        for (var prop in props) {
            Object.defineProperty(obj, prop, props[prop]);
        }
    };
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = start || 0, j = this.length; i < j; i++) {
            if (this[i] === obj) {
                return i;
            }
        }
        return -1;
    };
}

(function () {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () {
                callback(currTime + timeToCall);
            }, timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }
})();

/*! @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js*/
;if ("document" in self && !("classList" in document.createElement("_"))) {
    (function (j) {
        "use strict";
        if (!("Element" in j)) {
            return;
        }
        var a = "classList",
            f = "prototype",
            m = j.Element[f],
            b = Object,
            k = String[f].trim || function () {
            return this.replace(/^\s+|\s+$/g, "");
        },
            c = Array[f].indexOf || function (q) {
            var p = 0,
                o = this.length;
            for (; p < o; p++) {
                if (p in this && this[p] === q) {
                    return p;
                }
            }
            return -1;
        },
            n = function n(o, p) {
            this.name = o;
            this.code = DOMException[o];
            this.message = p;
        },
            g = function g(p, o) {
            if (o === "") {
                throw new n("SYNTAX_ERR", "An invalid or illegal string was specified");
            }
            if (/\s/.test(o)) {
                throw new n("INVALID_CHARACTER_ERR", "String contains an invalid character");
            }
            return c.call(p, o);
        },
            d = function d(s) {
            var r = k.call(s.getAttribute("class") || ""),
                q = r ? r.split(/\s+/) : [],
                p = 0,
                o = q.length;
            for (; p < o; p++) {
                this.push(q[p]);
            }
            this._updateClassName = function () {
                s.setAttribute("class", this.toString());
            };
        },
            e = d[f] = [],
            i = function i() {
            return new d(this);
        };
        n[f] = Error[f];
        e.item = function (o) {
            return this[o] || null;
        };
        e.contains = function (o) {
            o += "";
            return g(this, o) !== -1;
        };
        e.add = function () {
            var s = arguments,
                r = 0,
                p = s.length,
                q,
                o = false;
            do {
                q = s[r] + "";
                if (g(this, q) === -1) {
                    this.push(q);
                    o = true;
                }
            } while (++r < p);
            if (o) {
                this._updateClassName();
            }
        };
        e.remove = function () {
            var t = arguments,
                s = 0,
                p = t.length,
                r,
                o = false;
            do {
                r = t[s] + "";
                var q = g(this, r);
                if (q !== -1) {
                    this.splice(q, 1);
                    o = true;
                }
            } while (++s < p);
            if (o) {
                this._updateClassName();
            }
        };
        e.toggle = function (p, q) {
            p += "";
            var o = this.contains(p),
                r = o ? q !== true && "remove" : q !== false && "add";
            if (r) {
                this[r](p);
            }
            return !o;
        };
        e.toString = function () {
            return this.join(" ");
        };
        if (b.defineProperty) {
            var l = { get: i, enumerable: true, configurable: true };
            try {
                b.defineProperty(m, a, l);
            } catch (h) {
                if (h.number === -2146823252) {
                    l.enumerable = false;
                    b.defineProperty(m, a, l);
                }
            }
        } else {
            if (b[f].__defineGetter__) {
                m.__defineGetter__(a, i);
            }
        }
    })(self);
}
;
SL.util.Resize = (function () {
    function Resize() {
        _classCallCheck(this, Resize);
    }

    _createClass(Resize, null, [{
        key: "letterBox",

        /**
         * @param elW
         * @param elH
         * @param contW
         * @param contH
         * @returns {{x: number, y: number, w: number, h: number}}
         */
        value: function letterBox(elW, elH, contW, contH) {
            var elRatio = elW / elH;
            var contRatio = contW / contH;
            var pos = {
                x: 0,
                y: 0,
                w: 0,
                h: 0
            };

            if (elRatio < contRatio) {
                pos.w = contW;
                pos.h = Math.round(pos.w / elRatio);
                pos.y = Math.round(-(pos.h - contH) / 2);
            } else {
                pos.h = contH;
                pos.w = Math.round(pos.h * elRatio);
                pos.x = Math.round(-(pos.w - contW) / 2);
            }

            return pos;
        }
    }]);

    return Resize;
})();
SL.util.Scroll = (function () {
    function Scroll() {
        _classCallCheck(this, Scroll);
    }

    _createClass(Scroll, null, [{
        key: "easeTo",
        value: function easeTo(scrollX, scrollY, callback) {
            TweenLite.to({ scrollX: window.pageXOffset, scrollY: window.pageYOffset }, 0.8, {
                scrollX: scrollX, scrollY: scrollY, onUpdate: function onUpdate() {
                    window.scrollTo(this.target.scrollX, this.target.scrollY);
                }, onComplete: callback
            });
        }
    }]);

    return Scroll;
})();
SL.util.String = (function () {
    function String() {
        _classCallCheck(this, String);
    }

    _createClass(String, null, [{
        key: "ucfirst",
        value: function ucfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    }]);

    return String;
})();
SL.util.Validator = (function () {
    function Validator() {
        _classCallCheck(this, Validator);
    }

    _createClass(Validator, null, [{
        key: "notempty",
        value: function notempty(input) {
            var value = input.value;
            var $input = $(input);
            var check = value && value.trim().length > 0 && value != $input.attr("placeholder");

            $input.removeClass("error");
            if (!check) $input.addClass("error");
            return check;
        }
    }, {
        key: "email",
        value: function email(input) {
            var value = input.value;
            var $input = $(input);
            var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var check = regex.test(value);

            $input.removeClass("error");
            if (!check) $input.addClass("error");
            return check;
        }
    }, {
        key: "zip",
        value: function zip(input) {
            var value = input.value;
            var $input = $(input);
            var regex = /^[0-9]{5}$/;;
            var check = regex.test(value);

            $input.removeClass("error");
            if (!check) $input.addClass("error");
            return check;
        }
    }]);

    return Validator;
})();
/**
 *
 * @returns {Controller|*}
 */
function getController() {
    if (SL.main && SL.main.page) {
        return SL.main.page.controller;
    }
}
SL.manager.Event = (function () {
    function Event() {
        _classCallCheck(this, Event);
    }

    _createClass(Event, null, [{
        key: "on",

        /**
         * Add eventlistener
         * @param event
         * @param target
         * @param method
         * @param key
         * @param groupKey
         */
        value: function on(event, target, method, key) {
            var groupKey = arguments.length <= 4 || arguments[4] === undefined ? "global" : arguments[4];

            var e = {
                id: key,
                target: target,
                event: event,
                groupId: groupKey,
                method: method
            };

            if (!SL.manager.Event.groups[groupKey]) {
                SL.manager.Event.groups[groupKey] = {};
            }
            if (!SL.manager.Event.groups[groupKey][key]) {
                SL.manager.Event.groups[groupKey][key] = {};
            }
            SL.manager.Event.groups[groupKey][key][event] = e;

            $(target).on(event, method);
        }

        /**
         * Remove event listener
         * @param event
         * @param key
         * @param groupKey
         */
    }, {
        key: "off",
        value: function off(event, key) {
            var groupKey = arguments.length <= 2 || arguments[2] === undefined ? "global" : arguments[2];

            var e = SL.manager.Event.groups[groupKey][key][event];

            $(e.target).off(e.event, e.method);
            delete SL.manager.Event.groups[groupKey][key][event];
        }

        /**
         * Remove all listener for an event group
         * @param groupKey
         */
    }, {
        key: "groupOff",
        value: function groupOff(groupKey) {
            var id = "";
            var event = "";
            var e = null;
            for (id in SL.manager.Event.groups[groupKey]) {

                for (event in SL.manager.Event.groups[groupKey][id]) {
                    e = SL.manager.Event.groups[groupKey][id][event];
                    $(e.target).off(e.event, e.method);
                }
            }
            delete SL.manager.Event.groups[groupKey];
        }
    }]);

    return Event;
})();
SL.manager.Event.groups = {};
SL.manager.Loader = (function () {
    function Loader() {
        _classCallCheck(this, Loader);

        this.queue = new createjs.LoadQueue();
        this.queue.on("complete", this.onComplete, this);
        this.queue.on("progress", this.onProgress, this);
        this.loaded = 0;
    }

    _createClass(Loader, [{
        key: "loadContent",
        value: function loadContent() {
            var progress = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
            var complete = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

            var manifest = [];
            $("#content").find("img").each(function () {
                manifest.push({ id: this.id, src: this.getAttribute("src"), types: "image" });
            });

            this.load(manifest, progress, complete);
        }
    }, {
        key: "loadManifest",
        value: function loadManifest(manifest, progress, complete) {
            this.load(manifest, progress, complete);
        }
    }, {
        key: "load",
        value: function load(manifest, progress, complete) {
            this.loaded = 0;
            this.progress = progress ? progress : this.progress;
            this.complete = complete ? complete : this.complete;

            if (manifest.length === 0) {
                this.loaded = 1;
                this.complete();
            } else {
                this.queue.loadManifest(manifest);
            }
        }
    }, {
        key: "onProgress",
        value: function onProgress(e) {
            this.loaded = e.loaded;
            this.progress();
        }
    }, {
        key: "progress",
        value: function progress() {
            console.log(this.loaded);
        }
    }, {
        key: "onComplete",
        value: function onComplete(e) {
            this.complete();
        }
    }, {
        key: "complete",
        value: function complete() {
            console.log("complete load");
        }
    }]);

    return Loader;
})();
SL.partial.CookiePolicy = (function () {
    function CookiePolicy() {
        _classCallCheck(this, CookiePolicy);

        this.html = document.getElementById('cookie');
        this.bt = document.getElementById('cookie-bt');
        this.name = SL.main.path.getBase() + "cookiepolicy";
        if (SL.util.Cookie.readCookie(this.name) != null) {
            this.close();
        } else {
            this.html.style.display = "block";
        }

        $(this.bt).on(EVENT.click, this.close.bind(this));
    }

    _createClass(CookiePolicy, [{
        key: "close",
        value: function close() {
            SL.util.Cookie.createCookie(this.name, "", 395);
            this.html.parentNode.removeChild(this.html);
        }
    }]);

    return CookiePolicy;
})();
SL.partial.Footer = (function () {
    function Footer() {
        _classCallCheck(this, Footer);

        this.step = 0;
        this.setDom();
        this.addListeners();
    }

    _createClass(Footer, [{
        key: "setDom",
        value: function setDom() {
            this.html = {};
            this.html.footer = document.querySelector(".footer");
            this.html.contact = document.querySelector(".footer-contact");
            this.html.num = document.querySelector(".footer-contact-stepper-num-current");
            this.html.mentions = document.getElementById("#footer-contact-credits-mentions-link");
        }
    }, {
        key: "addListeners",
        value: function addListeners() {
            $(this.html.mentions).on(EVENT.click, SL.main.onClickPageLink);
            $(".footer-contact-stepper-bt").on(EVENT.click, this.onClickNext);
            $(".footer-contact-form-btback").on(EVENT.click, this.onClickBack);
        }
    }, {
        key: "onClickNext",
        value: function onClickNext(e) {
            if (!SL.main.page.footer.validStep()) return;
            SL.main.page.footer.step++;
            SL.main.page.footer.onStepChange();
        }
    }, {
        key: "onClickBack",
        value: function onClickBack(e) {
            SL.main.page.footer.step--;
            SL.main.page.footer.onStepChange();
        }
    }, {
        key: "onClickSend",
        value: function onClickSend(e) {
            if (!SL.main.page.footer.validStep()) return;

            var post = $(".footer-contact-form").serialize();
            SL.app.Api.sendContact(post, SL.main.page.footer.onSendContact, SL.main.page.footer);
        }
    }, {
        key: "onSendContact",
        value: function onSendContact(rs) {
            var resultat = JSON.parse(rs);
            if (resultat.status === 1) {
                $(".footer-contact-form").hide();
                $(".footer-contact-confirm").css("display", "inline-block");
                this.html.footer.className = "footer";
                this.html.contact.className = "footer-contact";
            } else {
                console.log(rs);
            }
        }
    }, {
        key: "onStepChange",
        value: function onStepChange() {
            $(".footer-contact-form-step").removeClass("footer-contact-form-step-selected");
            $("#footer-contact-form-step-" + this.step).addClass("footer-contact-form-step-selected");
            $(this.html.num).html(this.step + 1);

            if (this.step === 0) {
                $(".footer-contact-form-btback").hide();
            } else {
                $(".footer-contact-form-btback").show();
            }

            $(".footer-contact-stepper-bt").off(EVENT.click, this.onClickNext);
            $(".footer-contact-stepper-bt").off(EVENT.click, this.onClickSend);
            if (this.step === 2) {
                this.html.footer.className = "footer footer-step-2";
                this.html.contact.className = "footer-contact footer-contact-step-2";
                $(".footer-contact-stepper-bt-label").html("Envoyer");
                $(".footer-contact-stepper-bt").on(EVENT.click, this.onClickSend);
            } else {
                $(".footer-contact-stepper-bt-label").html("Suivant");
                $(".footer-contact-stepper-bt").on(EVENT.click, this.onClickNext);
                this.html.footer.className = "footer";
                this.html.contact.className = "footer-contact";
            }
        }
    }, {
        key: "validStep",
        value: function validStep() {
            var check = true;
            var inputs = $("#footer-contact-form-step-" + this.step).find("input,textarea");

            var lg = inputs.length;
            for (var i = 0; i < lg; i++) {
                check = !SL.util.Validator.notempty(inputs[i]) ? false : check;
                if (inputs[i].name === "email") {
                    check = !SL.util.Validator.email(inputs[i]) ? false : check;
                } else if (inputs[i].name === "zip") {
                    check = !SL.util.Validator.zip(inputs[i]) ? false : check;
                }
            }

            return check;
        }
    }]);

    return Footer;
})();
SL.partial.Header = (function () {
    function Header() {
        _classCallCheck(this, Header);

        this.html = document.querySelector(".header");
    }

    _createClass(Header, [{
        key: "addListeners",
        value: function addListeners() {}
    }, {
        key: "onChangePage",
        value: function onChangePage() {
            //SL.main.page.id
        }
    }]);

    return Header;
})();
SL.partial.Loading = (function () {
    function Loading() {
        _classCallCheck(this, Loading);

        this.percent = 0;
        this.html = document.createElement("div");
        document.body.appendChild(this.html);
        this.html.style.fontSize = "36px";
        this.html.style.color = "#000";
        this.html.style.padding = "20px";
        this.html.style.overflow = "hidden";
        this.tl = new TimelineLite();
        this.tl.stop();
        this.tl.fromTo(this.html, 0.6, { position: "absolute", display: "block", left: 0, top: 0, width: "calc(100% - 40px)", height: 50, backgroundColor: "#fff" }, { height: "calc(100% - 40px)", ease: Quart.easeOut });
    }

    _createClass(Loading, [{
        key: "reset",
        value: function reset() {
            this.setPercent(0);
        }
    }, {
        key: "show",
        value: function show(callback) {
            TweenLite.fromTo(this.html, 0.4, { width: 0, height: 0, display: "block", padding: "0 20px" }, {
                width: "calc(100% - 40px)", padding: 20, height: 50, ease: Quart.easeOut, onComplete: function onComplete() {
                    if (callback) callback();
                }
            });
        }
    }, {
        key: "hide",
        value: function hide() {
            var callback = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

            TweenLite.killTweensOf(this.html);
            TweenLite.to(this.html, 0.8, {
                height: 0, padding: "0 20px", delay: 0.6, ease: Quart.easeOut, onComplete: function onComplete() {
                    if (callback) callback();
                }
            });
        }
    }, {
        key: "setPercent",
        value: function setPercent(percent) {
            this.percent = percent;
            this.html.innerHTML = parseInt(SL.main.page.loader.loaded * 100).toString();
            this.tl.progress(percent);
        }
    }, {
        key: "clean",
        value: function clean() {
            this.tl.kill();
            document.removeChild(this.html);
            for (var s in this) {
                this[s] = null;
            }
        }
    }]);

    return Loading;
})();
SL.partial.Map = (function () {
    function Map(id) {
        _classCallCheck(this, Map);

        this.id = id;
        this.html = {};
        this.setDom();

        this.lat = parseFloat(this.html.map.getAttribute('data-lat'));
        this.lng = parseFloat(this.html.map.getAttribute('data-lng'));

        this.map = null;
        this.markers = [];
        this.infowindow = null;
        this.service = null;
        this.types = [];
        this.keywords = [];

        this.style = [{ "visibility": "simplified" }, {
            "featureType": "landscape",
            "stylers": [{ "color": "#e9e5dc" }]
        }, {
            "featureType": "poi",
            "stylers": [{ "color": "#ffffff" }]
        }, {
            "featureType": "road",
            "stylers": [{ "color": "#ffffff" }]
        }, {
            "featureType": "water",
            "stylers": [{ "color": "#8ef1dd" }]
        }, {
            "elementType": "labels.text.fill",
            "stylers": [{ "color": "#000000" }]
        }];

        this.options = {
            zoom: 16,
            disableDefaultUI: true,
            zoomControl: true,
            panControl: true,
            scrollwheel: false,
            draggable: true,
            styles: this.style
        };

        this.addScript();

        this.icons = {
            "shop": {},
            "school": {},
            "spectacle": {}
        };
    }

    _createClass(Map, [{
        key: "addScript",
        value: function addScript() {
            if (window["google"] === undefined || window["google"].maps === undefined) {
                var script = document.createElement('script');
                script.types = 'text/javascript';
                script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&callback=SL.main.page.controller.map.init';
                document.body.appendChild(script);
            } else {
                this.init();
            }
        }
    }, {
        key: "init",
        value: function init() {
            this.addListeners();

            this.center = new google.maps.LatLng(this.lat, this.lng);

            this.map = new google.maps.Map(this.html.map, this.options);
            this.map.setCenter(this.center);

            this.infowindow = new google.maps.InfoWindow();

            this.service = new google.maps.places.PlacesService(this.map);
        }
    }, {
        key: "setDom",
        value: function setDom() {
            this.html.map = document.getElementById(this.id);
        }
    }, {
        key: "addListeners",
        value: function addListeners() {}
    }, {
        key: "removeListeners",
        value: function removeListeners() {}
    }, {
        key: "getIcon",
        value: function getIcon(place) {
            var rs = null;
            var icon = "default";
            var name = place.name.toLowerCase();
            var types = place.types;
            if (types.indexOf("post_office") != -1) {
                icon = "poste";
            } else if (types.indexOf("grocery_or_supermarket") != -1) {
                icon = "shop";
            } else if (types.indexOf("school") != -1) {
                icon = "school";
            } else if (types.indexOf("movie_theater") != -1) {
                icon = "cinema";
            } else if (types.indexOf("gym") != -1) {
                icon = "sport";
            } else if (types.indexOf("bar") != -1) {
                icon = "bar";
            } else if (types.indexOf("restaurant") != -1) {
                icon = "restaurant";
            } else if (types.indexOf("hospital") != -1) {
                icon = "hospital";
            } else if (types.indexOf("pharmacy") != -1) {
                icon = "pharmacie";
            } else if (types.indexOf("bus_station") != -1) {
                icon = "bus";
            } else if (types.indexOf("subway_station") != -1 || types.indexOf("transit_station") != -1) {
                icon = "metro";
            }

            if (name.indexOf("marché") != -1) {
                icon = "shop";
            } else if (name.indexOf("bibliothèque") != -1) {
                icon = "bibliotheque";
            } else if (name.indexOf("monument") != -1 || name.indexOf("historique") != -1 || name.indexOf("mémorial") != -1) {
                icon = "monument";
            } else if (name.indexOf("théâtre") != -1 || name.indexOf("theatre") != -1) {
                icon = "spectacle";
            } else if (name.indexOf("crèche") != -1 || name.indexOf("creche") != -1) {
                icon = "creche";
            } else if (name.indexOf("maternelle") != -1) {
                icon = "maternelle";
            } else if (name.indexOf("école primaire") != -1 || name.indexOf("ecole primaire") != -1) {
                icon = "school";
            } else if (name.indexOf("collège") != -1 || name.indexOf("college") != -1) {
                icon = "colleges";
            } else if (name.indexOf("lycee") != -1 || name.indexOf("lycee") != -1) {
                icon = "colleges";
            } else if (name.indexOf("enseignement") != -1 || name.indexOf("facul") != -1 || name.indexOf("universit") != -1) {
                icon = "colleges";
            } else if (name.indexOf("tramway") != -1) {
                icon = "metro";
            } else if (name.indexOf("vélo") != -1) {
                icon = "velo";
            }

            var anchor = new google.maps.Point(0, 61);
            var size = new google.maps.Size(62, 61);
            rs = {
                url: SL.main.path.getAssetsUrl() + "svg/map/" + icon + ".svg",
                anchor: anchor,
                scaledSize: size
            };
            return rs;
        }
    }, {
        key: "onSearchNearBy",
        value: function onSearchNearBy(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    this.createMarker(results[i]);
                }
            }
        }
    }, {
        key: "createMarker",
        value: function createMarker(place) {

            var icon = this.getIcon(place);
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map: this.map,
                position: place.geometry.location,
                icon: icon
            });

            google.maps.event.addListener(marker, 'click', function () {
                SL.main.page.controller.map.showInfoWindow(place, this);
            });

            this.markers.push(marker);
        }
    }, {
        key: "clearMarkers",
        value: function clearMarkers() {
            var i = 0;
            var lg = this.markers.length;
            for (i = 0; i < lg; i++) {
                this.markers[i].setMap(null);
            }
        }
    }, {
        key: "showInfoWindow",
        value: function showInfoWindow(place, marker) {
            console.log(place);
            if (place.name && place.name != "" && place.name != " ") {
                this.infowindow.setContent('<div class="map-infowindow">' + place.name + '<br>' + place.vicinity + '</div>');
                this.infowindow.open(this.map, marker);
            }
        }

        /**
         * addPlaces
         * @param types array
         * @param keywords array
         */
    }, {
        key: "addPlaces",
        value: function addPlaces(types, keywords) {
            this.clearMarkers();

            var i = 0;
            var lg = types.length;
            for (i = 0; i < lg; i++) {
                if (types[i] != '' && this.types.indexOf(types[i]) === -1) {
                    this.types.push(types[i]);
                }
            }

            lg = keywords.length;
            for (i = 0; i < lg; i++) {
                if (keywords[i] != '' && this.keywords.indexOf(keywords[i]) === -1) {
                    this.keywords.push(keywords[i]);
                }
            }
            this.searchNearBy();
        }
    }, {
        key: "removePlaces",
        value: function removePlaces(types, keywords) {

            var at = null;
            var i = 0;
            var lg = types.length;
            for (i = 0; i < lg; i++) {
                at = this.types.indexOf(types[i]);
                if (at != -1) this.types.splice(at, 1);
            }

            lg = keywords.length;
            for (i = 0; i < lg; i++) {
                at = this.keywords.indexOf(keywords[i]);
                if (at != -1) this.keywords.splice(at, 1);
            }

            this.clearMarkers();
            this.searchNearBy();
        }
    }, {
        key: "searchNearBy",
        value: function searchNearBy() {

            var requests = [];
            var request = null;
            var i = 0;
            var lg = this.types.length;
            for (i = 0; i < lg; i++) {
                request = {
                    location: this.center,
                    radius: '2000', //meters,
                    keyword: '',
                    types: [this.types[i]]
                };
                requests.push(request);
                //this.service.nearbySearch(this.request, this.onSearchNearBy.bind(this));
            }

            lg = this.keywords.length;
            var keyword = "";
            for (i = 0; i < lg; i++) {
                request = {
                    location: this.center,
                    radius: '2000', //meters,
                    keyword: this.keywords[i],
                    types: []
                };
                requests.push(request);
                //this.service.nearbySearch(this.request, this.onSearchNearBy.bind(this));
            }
            lg = requests.length;
            for (i = 0; i < lg; i++) {
                setTimeout(function (request) {
                    SL.main.page.controller.map.service.nearbySearch(request, SL.main.page.controller.map.onSearchNearBy.bind(SL.main.page.controller.map));
                }, i * 100, requests[i]);
            }
        }
    }, {
        key: "clean",
        value: function clean() {
            for (var s in this) {
                this[s] = null;
            }
        }
    }]);

    return Map;
})();
SL.partial.Slider = (function () {
    function Slider(id) {
        var onChange = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];
        var total = arguments.length <= 2 || arguments[2] === undefined ? 1 : arguments[2];

        _classCallCheck(this, Slider);

        this.id = id;
        this.onChange = onChange;
        this.page = 0;
        this.sens = 0;
        this.prefix = "/";
        this.total = total;
        this.canSlide = true;
        this.html = {};
        this.setDom();
        this.addListeners();

        if (this.html.container) {
            this.total = this.html.container.getAttribute("data-total") ? parseInt(this.html.container.getAttribute("data-total")) : this.total;

            if (this.total <= 1) {
                this.html.container.style.display = "none";
            }
        }
    }

    _createClass(Slider, [{
        key: "setDom",
        value: function setDom() {
            this.html.container = document.getElementById(this.id);
            this.html.label = document.getElementById(this.id + '-label');
            this.html.prev = document.getElementById(this.id + '-bt-prev');
            this.html.next = document.getElementById(this.id + '-bt-next');
        }
    }, {
        key: "addListeners",
        value: function addListeners() {
            $(this.html.prev).on(EVENT.click, this.onPagination.bind(this));
            $(this.html.next).on(EVENT.click, this.onPagination.bind(this));
        }
    }, {
        key: "removeListeners",
        value: function removeListeners() {
            $(this.html.prev).off(EVENT.click, this.onPagination.bind(this));
            $(this.html.next).off(EVENT.click, this.onPagination.bind(this));
        }
    }, {
        key: "onPagination",
        value: function onPagination(e) {
            this.sens = e.currentTarget === this.html.prev ? SENS_LESS : SENS_MORE;
            this.set();
        }
    }, {
        key: "set",
        value: function set() {
            if (!this.canSlide) return;
            this.canSlide = false;
            var page = this.page + this.sens;
            page = page >= this.total ? 0 : page;
            page = page < 0 ? this.total - 1 : page;

            if (page != this.page) {
                this.page = page;
                if (this.html.label) this.html.label.innerHTML = this.page + 1 + this.prefix + this.total;
                if (this.onChange) this.onChange(this.page, this.sens);
            }
        }
    }, {
        key: "clean",
        value: function clean() {
            this.removeListeners();

            for (var s in this) {
                this[s] = null;
            }
        }
    }]);

    return Slider;
})();
SL.controller.Controller = (function () {
    function Controller(id) {
        _classCallCheck(this, Controller);

        this.id = id;
        this.tws = {};
        this.tls = {};
        this.html = {};
        this.w = 0;
        this.h = 0;
    }

    _createClass(Controller, [{
        key: "init",
        value: function init() {
            this.setDom();
        }
    }, {
        key: "setDom",
        value: function setDom() {
            this.html.content = document.getElementById('content');
        }
    }, {
        key: "show",
        value: function show() {}
    }, {
        key: "hide",
        value: function hide() {}
    }, {
        key: "resize",
        value: function resize(w, h) {
            this.w = w;
            this.h = h;
        }
    }, {
        key: "loop",
        value: function loop() {}
    }, {
        key: "clean",
        value: function clean() {
            SL.manager.Event.groupOff(this.id);

            for (var s in this.tws) {
                this.tws[s].kill();
            }

            for (var s in this.tls) {
                this.tls[s].kill();
            }

            for (var s in this) {
                this[s] = null;
            }
        }
    }, {
        key: "onCaptchaScriptReady",
        value: function onCaptchaScriptReady() {
            //if needed override
        }
    }]);

    return Controller;
})();
SL.controller.Home = (function (_SL$controller$Controller) {
    _inherits(Home, _SL$controller$Controller);

    function Home(id) {
        _classCallCheck(this, Home);

        _get(Object.getPrototypeOf(Home.prototype), "constructor", this).call(this, id);
    }

    _createClass(Home, [{
        key: "init",
        value: function init() {
            _get(Object.getPrototypeOf(Home.prototype), "init", this).call(this);
            var target = document.getElementById('content');
            SL.manager.Event.on(EVENT.click, target, this.onClick, "content", this.id);
        }
    }, {
        key: "onClick",
        value: function onClick(e) {
            console.log("onClick");
            SL.manager.Event.off(EVENT.click, 'content', getController().id);
        }
    }]);

    return Home;
})(SL.controller.Controller);
SL.controller.Page404 = (function (_SL$controller$Controller2) {
    _inherits(Page404, _SL$controller$Controller2);

    function Page404(id) {
        _classCallCheck(this, Page404);

        _get(Object.getPrototypeOf(Page404.prototype), "constructor", this).call(this, id);
    }

    return Page404;
})(SL.controller.Controller);
SL.controller.Test = (function (_SL$controller$Controller3) {
    _inherits(Test, _SL$controller$Controller3);

    function Test(id) {
        _classCallCheck(this, Test);

        _get(Object.getPrototypeOf(Test.prototype), "constructor", this).call(this, id);
    }

    return Test;
})(SL.controller.Controller);
SL.app.Api = (function () {
    function Api() {
        _classCallCheck(this, Api);
    }

    _createClass(Api, null, [{
        key: "sendContact",
        value: function sendContact(post, callback, context) {
            var url = SL.main.path.getApiUrl() + "contact/send";
            SL.app.Api.post(url, post, callback, context);
        }
    }, {
        key: "sendTerrain",
        value: function sendTerrain(post, callback, context) {
            var url = SL.main.path.getApiUrl() + "terrain/send";
            SL.app.Api.post(url, post, callback, context);
        }
    }, {
        key: "post",
        value: function post(url, _post, callback, context) {
            $.ajax({
                types: 'POST',
                url: url,
                data: _post,
                timeout: 0,
                context: context,
                success: callback,
                error: function error(xhr, type) {
                    //TODO alert('Ajax error!')
                }
            });
        }
    }]);

    return Api;
})();
SL.app.Lang = (function () {
    function Lang() {
        _classCallCheck(this, Lang);

        this.list = SL.main.config.lang.list;
        this["default"] = SL.main.config.lang["default"];
        this.setName();
    }

    _createClass(Lang, [{
        key: "setName",
        value: function setName() {
            var path = SL.main.path;
            this.name = SL.main.config.lang["default"];
            if (this.list.length > 0) {
                if (path.getCurrent() != path.getBase()) {
                    this.name = path.getCurrent().replace(path.getBase(), "");
                    this.name = this.name.split("/");
                    this.name = this.name[0];
                }
                if (this.name.indexOf("?") != -1) {
                    this.name = this.name.split('?');
                    this.name = this.name[0];
                }

                if (!this.checkLang()) {
                    this.name = SL.main.config.lang["default"];
                }
            }
        }
    }, {
        key: "checkLang",
        value: function checkLang() {
            return this.list.indexOf(this.name) != -1;
        }
    }]);

    return Lang;
})();
SL.app.Page = (function () {
    function Page() {
        _classCallCheck(this, Page);

        this.content = document.getElementById('content');
        this.id = null;
        this.params = null;
        /**
         * @type Controller
         */
        this.controller = null;
        this.local = null;
        /**
         * @type Footer
         */
        this.footer = null;
        /**
         * @type Header
         */
        this.header = null;
        /**
         * @type Loader
         */
        this.loader = null;
        this.isTransitionning = false;
        this.isContentReady = false;
        this.loader = new SL.manager.Loader();
        this.tl = null;
        /**
         * @type Loading
         */
        this.loading = null;
        this.w = 0;
        this.h = 0;
        this.mousex = 0;
        this.mousey = 0;

        this.cookie = SL.main.lang.name === "fr" ? new SL.partial.CookiePolicy() : null;
    }

    _createClass(Page, [{
        key: "addListeners",
        value: function addListeners() {
            $(window).on(EVENT.resize, this.resize);
            $(document).on(EVENT.mousemove, this.mousemove);
            document.addEventListener(EVENT.touchmove, this.touchmove, false);
            requestAnimationFrame(SL.main.page.loop);
        }
    }, {
        key: "loop",
        value: function loop() {
            if (SL.main.page.controller) SL.main.page.controller.loop();
            if (SL.main.page.header && SL.main.page.header.loop) SL.main.page.header.loop();
            requestAnimationFrame(SL.main.page.loop);
        }
    }, {
        key: "mousemove",
        value: function mousemove(e) {
            SL.main.page.mousex = e.clientX;
            SL.main.page.mousey = e.clientY;
        }
    }, {
        key: "touchmove",
        value: function touchmove(e) {
            SL.main.page.mousex = e.touches[0].clientX;
            SL.main.page.mousey = e.touches[0].clientY;
        }
    }, {
        key: "resize",
        value: function resize(e) {
            SL.main.page.w = document.documentElement.clientWidth;
            SL.main.page.h = document.documentElement.clientHeight;
            if (SL.main.page.controller) SL.main.page.controller.resize(SL.main.page.w, SL.main.page.h);
        }
    }, {
        key: "init",
        value: function init() {
            this.addListeners();
            this.footer = new SL.partial.Footer();
            this.header = new SL.partial.Header();

            this.loading = null; //new SL.partial.Loading();
            this.loader = null; //new SL.app.Loader();

            this.setCurrent();
        }
    }, {
        key: "setCurrent",
        value: function setCurrent() {
            this.id = SL.main.route.current.id;
            this.params = SL.main.route.current.params;
            this.local = SL.main.route.current.local;
            this.header.onChangePage();
            this.setController();
        }
    }, {
        key: "setController",
        value: function setController() {
            if (!this.controller) {
                this.controller = new SL.controller[SL.main.route.current.controller.js](SL.main.route.current.id);
                this.controller.init();
                this.isContentReady = true;
                this.transitionIn();
            } else {
                this.isContentReady = false;
                this.transitionOut();
            }
        }
    }, {
        key: "progress",
        value: function progress() {
            //console.log(loaded);
            SL.main.page.loading.setPercent(SL.main.page.loader.loaded);
        }
    }, {
        key: "complete",
        value: function complete() {
            SL.main.page.loading.setPercent(100);
            SL.main.page.loading.hide();
            SL.main.page.transitionIn();
        }
    }, {
        key: "checkContent",
        value: function checkContent() {
            var currentClass = this.content.querySelector("div").className;
            if (currentClass.indexOf(SL.main.route.current.id) === -1) {
                this.setCurrent();
            }
        }
    }, {
        key: "transitionIn",
        value: function transitionIn() {
            if (this.isTransitionning) return;
            this.resize();
            this.isTransitionning = true;
            TweenLite.to("#content", 0.4, {
                opacity: 1, ease: Quad.easeOut, onComplete: function onComplete() {
                    SL.main.page.isTransitionning = false;
                    SL.main.page.checkContent();
                    SL.main.page.resize();
                }
            });
            this.controller.show();
        }
    }, {
        key: "transitionOut",
        value: function transitionOut() {
            if (this.isTransitionning) return;
            SL.main.page.isTransitionning = true;
            TweenLite.to("#content", 0.4, { opacity: 0, ease: Quad.easeOut, onComplete: SL.main.page.transitionOutComplete });
            this.controller.hide();

            SL.util.Scroll.easeTo(0, 0);
        }
    }, {
        key: "transitionOutComplete",
        value: function transitionOutComplete() {
            SL.main.page.isTransitionning = false;
            SL.main.page.controller.clean();
            SL.util.File.getPage(SL.main.path.getCurrent(), SL.main.page.onGetPage, SL.main.page);
        }
    }, {
        key: "onGetPage",
        value: function onGetPage(response) {
            if (response) {
                this.isContentReady = true;
                this.content.innerHTML = response;
                this.controller = new SL.controller[SL.main.route.current.controller.js](SL.main.route.current.id);
                this.controller.init();
                this.transitionIn();
            } else {
                SL.util.File.getPage(SL.main.path.getBase() + "page404", SL.main.page.onGetPage, SL.main.page);
            }
        }
    }, {
        key: "load",
        value: function load() {
            SL.main.page.loading.reset();
            SL.main.page.loading.show(SL.main.page.onload);
        }
    }, {
        key: "onload",
        value: function onload() {
            SL.main.page.loader.loadContent(SL.main.page.progress, SL.main.page.complete);
        }
    }, {
        key: "show",
        value: function show() {
            if (this.isTransitionning || !this.isContentReady) return;
            this.transitionIn();
        }
    }]);

    return Page;
})();
SL.app.Path = (function () {
    function Path() {
        _classCallCheck(this, Path);

        this.url = {};
        this.setDevice();
        this.setUrl();
    }

    _createClass(Path, [{
        key: "setDevice",
        value: function setDevice() {
            //Detect device type (mobile or desktop)
            this.device = SL.main.config.devices.list[0];
            Detectizr.detect({ detectScreen: false });
            if (SL.main.config.devices.list.indexOf(Detectizr.device.types) != -1) this.device = Detectizr.device.types;
            this.device = SL.main.config.devices.forced ? SL.main.config.devices.forced : this.device;
        }
    }, {
        key: "setUrl",
        value: function setUrl() {
            //Set urls
            this.url.base = SL.main.config.env.url.base;
            this.url.device = this.url.base + this.device + "/";
            this.url.assets = this.url.device + "assets/";
            this.url.current = window.location.href;
            this.url.local = this.url.device + "local/";
            this.url.tpl = this.url.device + "tpl/";
            this.url.page = this.url.tpl + "page/";
            this.url.partial = this.url.tpl + "partial/";
            this.url.cms = SL.main.config.env.url.cms;
            this.url.api = this.url.cms + "api/";
        }
    }, {
        key: "getCurrent",
        value: function getCurrent() {
            return this.url.current;
        }
    }, {
        key: "setCurrent",
        value: function setCurrent(current) {
            this.url.current = current;
        }
    }, {
        key: "getBase",
        value: function getBase() {
            return this.url.base;
        }
    }, {
        key: "getAssetsUrl",
        value: function getAssetsUrl() {
            return this.url.assets;
        }
    }, {
        key: "getDeviceUrl",
        value: function getDeviceUrl() {
            return this.url.device;
        }
    }, {
        key: "getApiUrl",
        value: function getApiUrl() {
            return this.url.api;
        }
    }]);

    return Path;
})();
SL.app.Route = (function () {
    function Route() {
        _classCallCheck(this, Route);

        this.routes = SL.main.config.routes;
        this.semantics = [];
        this.routes = {};
        this.current = null;
        this.urls = {};
        this.page = null;

        this.setRoutes();
        this.setCurrent();
    }

    _createClass(Route, [{
        key: "setRoutes",
        value: function setRoutes() {
            var lang = SL.main.lang;
            var path = SL.main.path;

            for (var k in SL.main.config.routes) {
                var route = SL.util.Object.clone(SL.main.config.routes[k]);
                var local = route.local;
                this.semantics[k] = local[lang.name].semantic;

                //Set alternates routes
                var lg = lang.list.length;
                for (var i = 0; i < lg; i++) {
                    var l = lang.list[i];
                    if (!this.urls[l]) this.urls[l] = {};
                    this.urls[l][k] = path.url.base + l + "/" + local[l].semantic;
                }

                route.local = local[lang.name];
                this.routes[k] = route;
            }

            this.urls.assets = path.getAssetsUrl();
        }
    }, {
        key: "setCurrent",
        value: function setCurrent() {
            var lang = SL.main.lang;
            var path = SL.main.path;

            //Check lang
            if (!lang.checkLang()) location.href = path.getBase() + lang["default"];

            var tocheck = path.getCurrent();
            tocheck = tocheck.replace(path.getBase(), "");
            tocheck = tocheck.replace(lang.name + "/", "");

            if (tocheck.indexOf("?") != -1) {
                tocheck = tocheck.split("?");
                tocheck = tocheck[0];
            }

            var params = tocheck.split("/");
            tocheck = params[0];
            params = params.slice(1);

            this.current = this.routes.page404;
            var id = SL.util.Object.keyOf(this.semantics, tocheck);
            id = id ? id : "page404";
            this.current = this.routes[id];

            if (params.length > 0 && this.current.params) {
                var p = {};
                var i = 0;
                if (Array.isArray(this.current.params)) {
                    var lg = params.length;
                    for (i; i < lg; i++) {
                        p[this.current.params[i]] = params[i];
                    }
                } else {
                    for (var s in this.current.params) {
                        p[s] = params[i];
                        i++;
                    }
                }
                this.current.params = p;
            }
            this.current.id = id;
            this.current.controller.js = SL.util.String.ucfirst(this.current.controller.js);
        }
    }]);

    return Route;
})();

SL.app.Main = (function () {
    function Main(baseurl) {
        _classCallCheck(this, Main);

        console.log('%c Salty lighty. Version: Basic Banana ', 'color: #2b2b2b; display: block; line-height: 24px;');

        this.config = null;
        /**
         * @type Path
         */
        this.path = null;
        /**
         * @type Lang
         */
        this.lang = null;
        /**
         * @type Route
         */
        this.route = null;
        /**
         * @type Page
         */
        this.page = null;
        var configUrl = baseurl + "config.json";
        SL.util.File.getFile(configUrl, this.onGetConfig, this);
    }

    _createClass(Main, [{
        key: "onGetConfig",
        value: function onGetConfig(response) {
            this.config = response;
            this.set();
            this.addListeners();
            this.addTest();
        }
    }, {
        key: "set",
        value: function set() {
            this.path = new SL.app.Path();
            this.lang = new SL.app.Lang();
            this.route = new SL.app.Route();
            this.page = new SL.app.Page();
            this.page.init();
        }
    }, {
        key: "addTest",
        value: function addTest() {
            if (!Modernizr.input.placeholder) {
                SL.util.File.getFile(this.path.getDeviceUrl() + 'js/oldBrowser/placeholders.min.js');
            }
        }
    }, {
        key: "addListeners",
        value: function addListeners() {
            if (this.config.options.ajax) {
                History.Adapter.bind(window, 'statechange', this.onChangeHistory);
            }
        }
    }, {
        key: "setUrl",
        value: function setUrl(url) {
            if (url === SL.main.path.getCurrent()) return;
            SL.main.path.setCurrent(url);
            var currentLang = SL.main.lang.name;
            SL.main.lang.setName();
            if (SL.main.lang.name != currentLang) {
                SL.main.route.setRoutes();
            }
            SL.main.route.setCurrent();
            SL.main.page.setCurrent();
        }
    }, {
        key: "onChangeHistory",
        value: function onChangeHistory() {
            var state = History.getState();
            if (SL.main.path.getCurrent() === state.url) return;
            SL.main.setUrl(state.url);
        }
    }, {
        key: "onClickPageLink",
        value: function onClickPageLink(e) {
            if (SL.main.config.options.ajax && e.preventDefault && Modernizr.history) {
                e.preventDefault();
                if (SL.main.page.header.mobileMenuOpened == true) SL.main.page.header.closeMobileMenu();
                SL.main.setUrl(this.href);

                var semantic = SL.main.page.local.semantic;

                History.pushState(null, SL.main.page.local.title, SL.main.path.getCurrent());

                //Analytics
                if (window["ga"]) {
                    var pageview = '/' + semantic;
                    ga('send', 'pageview', pageview);
                }
            } else {
                window.location.href = this.href;
            }
        }
    }]);

    return Main;
})();

callbackScriptLoaded();