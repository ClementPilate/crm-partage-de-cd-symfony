SL.controller.Controller = class Controller {
    constructor(id) {
        this.id = id;
        this.tws = {};
        this.tls = {};
        this.html = {};
        this.w = 0;
        this.h = 0;
    }

    init() {
        this.setDom();
    }

    setDom() {
        this.html.content = document.getElementById('content');
    }

    show() {
    }

    hide() {
    }

    resize(w, h) {
        this.w = w;
        this.h = h;
    }

    loop() {
    }

    clean() {
        SL.manager.Event.groupOff(this.id);

        for (let s in this.tws) {
            this.tws[s].kill();
        }

        for (let s in this.tls) {
            this.tls[s].kill();
        }

        for (let s in this) {
            this[s] = null;
        }
    }

    onCaptchaScriptReady(){
        //if needed override
    }
};