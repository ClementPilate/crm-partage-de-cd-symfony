SL.controller.Home = class Home extends SL.controller.Controller {
    constructor(id) {
        super(id);
    }

    init() {
        super.init();
        let target = document.getElementById('content');
        SL.manager.Event.on(EVENT.click, target, this.onClick, "content",this.id);
    }

    onClick(e) {
        console.log("onClick");
        SL.manager.Event.off(EVENT.click, 'content',getController().id);
    }

};