SL.app.Path = class Path {

    constructor() {
        this.url = {};
        this.setDevice();
        this.setUrl();
    }

    setDevice() {
        //Detect device type (mobile or desktop)
        this.device = SL.main.config.devices.list[0];
        Detectizr.detect({detectScreen: false});
        if (SL.main.config.devices.list.indexOf(Detectizr.device.types) != -1) this.device = Detectizr.device.types;
        this.device = SL.main.config.devices.forced ? SL.main.config.devices.forced : this.device;
    }

    setUrl() {
        //Set urls
        this.url.base = SL.main.config.env.url.base;
        this.url.device = this.url.base + this.device + "/";
        this.url.assets = this.url.device + "assets/";
        this.url.current = window.location.href;
        this.url.local = this.url.device + "local/";
        this.url.tpl = this.url.device + "tpl/";
        this.url.page = this.url.tpl + "page/";
        this.url.partial = this.url.tpl + "partial/";
        this.url.cms = SL.main.config.env.url.cms;
        this.url.api = this.url.cms + "api/";
    }

    getCurrent(){
        return this.url.current;
    }

    setCurrent(current){
        this.url.current = current;
    }

    getBase(){
        return this.url.base;
    }

    getAssetsUrl(){
        return this.url.assets;
    }

    getDeviceUrl(){
        return this.url.device;
    }

    getApiUrl(){
        return this.url.api;
    }
};