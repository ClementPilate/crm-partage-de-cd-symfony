SL.app.Page = class Page {
    constructor() {
        this.content = document.getElementById('content');
        this.id = null;
        this.params = null;
        /**
         * @type Controller
         */
        this.controller = null;
        this.local = null;
        /**
         * @type Footer
         */
        this.footer = null;
        /**
         * @type Header
         */
        this.header = null;
        /**
         * @type Loader
         */
        this.loader = null;
        this.isTransitionning = false;
        this.isContentReady = false;
        this.loader = new SL.manager.Loader();
        this.tl = null;
        /**
         * @type Loading
         */
        this.loading = null;
        this.w = 0;
        this.h = 0;
        this.mousex = 0;
        this.mousey = 0;

        this.cookie = SL.main.lang.name === "fr" ? new SL.partial.CookiePolicy() : null;
    }

    addListeners() {
        $(window).on(EVENT.resize, this.resize);
        $(document).on(EVENT.mousemove, this.mousemove);
        document.addEventListener(EVENT.touchmove, this.touchmove, false);
        requestAnimationFrame(SL.main.page.loop);
    }

    loop() {
        if (SL.main.page.controller)SL.main.page.controller.loop();
        if (SL.main.page.header && SL.main.page.header.loop)SL.main.page.header.loop();
        requestAnimationFrame(SL.main.page.loop);
    }

    mousemove(e) {
        SL.main.page.mousex = e.clientX;
        SL.main.page.mousey = e.clientY;
    }

    touchmove(e) {
        SL.main.page.mousex = e.touches[0].clientX;
        SL.main.page.mousey = e.touches[0].clientY;
    }

    resize(e) {
        SL.main.page.w = document.documentElement.clientWidth;
        SL.main.page.h = document.documentElement.clientHeight;
        if (SL.main.page.controller)SL.main.page.controller.resize(SL.main.page.w, SL.main.page.h);
    }

    init() {
        this.addListeners();
        this.footer = new SL.partial.Footer();
        this.header = new SL.partial.Header();

        this.loading = null;//new SL.partial.Loading();
        this.loader = null;//new SL.app.Loader();

        this.setCurrent();
    }

    setCurrent() {
        this.id = SL.main.route.current.id;
        this.params = SL.main.route.current.params;
        this.local = SL.main.route.current.local;
        this.header.onChangePage();
        this.setController();
    }

    setController() {
        if (!this.controller) {
            this.controller = new SL.controller[SL.main.route.current.controller.js](SL.main.route.current.id);
            this.controller.init();
            this.isContentReady = true;
            this.transitionIn();
        } else {
            this.isContentReady = false;
            this.transitionOut();
        }
    }

    progress() {
        //console.log(loaded);
        SL.main.page.loading.setPercent(SL.main.page.loader.loaded);
    }

    complete() {
        SL.main.page.loading.setPercent(100);
        SL.main.page.loading.hide();
        SL.main.page.transitionIn();
    }

    checkContent() {
        var currentClass = this.content.querySelector("div").className;
        if (currentClass.indexOf(SL.main.route.current.id) === -1) {
            this.setCurrent();
        }
    }

    transitionIn() {
        if (this.isTransitionning)return;
        this.resize();
        this.isTransitionning = true;
        TweenLite.to("#content", 0.4, {
            opacity: 1, ease: Quad.easeOut, onComplete: function () {
                SL.main.page.isTransitionning = false;
                SL.main.page.checkContent();
                SL.main.page.resize();
            }
        });
        this.controller.show();
    }

    transitionOut() {
        if (this.isTransitionning)return;
        SL.main.page.isTransitionning = true;
        TweenLite.to("#content", 0.4, {opacity: 0, ease: Quad.easeOut, onComplete: SL.main.page.transitionOutComplete});
        this.controller.hide();

        SL.util.Scroll.easeTo(0, 0);
    }

    transitionOutComplete() {
        SL.main.page.isTransitionning = false;
        SL.main.page.controller.clean();
        SL.util.File.getPage(SL.main.path.getCurrent(), SL.main.page.onGetPage, SL.main.page);
    }

    onGetPage(response) {
        if (response) {
            this.isContentReady = true;
            this.content.innerHTML = response;
            this.controller = new SL.controller[SL.main.route.current.controller.js](SL.main.route.current.id);
            this.controller.init();
            this.transitionIn();
        } else {
            SL.util.File.getPage(SL.main.path.getBase() + "page404", SL.main.page.onGetPage, SL.main.page);
        }

    }

    load() {
        SL.main.page.loading.reset();
        SL.main.page.loading.show(SL.main.page.onload);
    }

    onload() {
        SL.main.page.loader.loadContent(SL.main.page.progress, SL.main.page.complete);
    }

    show() {
        if (this.isTransitionning || !this.isContentReady)return;
        this.transitionIn();
    }

};