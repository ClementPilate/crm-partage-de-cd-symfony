SL.app.Route = class Route {
    constructor() {
        this.routes = SL.main.config.routes;
        this.semantics = [];
        this.routes = {};
        this.current = null;
        this.urls = {};
        this.page = null;

        this.setRoutes();
        this.setCurrent();
    }

    setRoutes() {
        let lang = SL.main.lang;
        let path = SL.main.path;
        
        for (let k in SL.main.config.routes) {
            let route = SL.util.Object.clone(SL.main.config.routes[k]);
            let local = route.local;
            this.semantics[k] = local[lang.name].semantic;

            //Set alternates routes
            let lg = lang.list.length;
            for (let i = 0; i < lg; i++) {
                let l = lang.list[i];
                if (!this.urls[l]) this.urls[l] = {};
                this.urls[l][k] = path.url.base + l + "/" + local[l].semantic;
            }

            route.local = local[lang.name];
            this.routes[k] = route;
        }

        this.urls.assets = path.getAssetsUrl();
    }

    setCurrent() {
        let lang = SL.main.lang;
        let path = SL.main.path;

        //Check lang
        if (!lang.checkLang()) location.href = path.getBase() + lang.default;

        let tocheck = path.getCurrent();
        tocheck = tocheck.replace(path.getBase(), "");
        tocheck = tocheck.replace(lang.name + "/", "");

        if (tocheck.indexOf("?") != -1) {
            tocheck = tocheck.split("?");
            tocheck = tocheck[0];
        }

        let params = tocheck.split("/");
        tocheck = params[0];
        params = params.slice(1);

        this.current = this.routes.page404;
        let id = SL.util.Object.keyOf(this.semantics, tocheck);
        id = id?id:"page404";
        this.current = this.routes[id];
        
        if (params.length > 0 && this.current.params) {
            let p = {};
            let i = 0;
            if(Array.isArray(this.current.params)){
                let lg = params.length;
                for (i; i < lg; i++) {
                    p[this.current.params[i]] = params[i];
                }
            }else{
                for(let s in this.current.params){
                    p[s] = params[i];
                    i++;
                }
            }
            this.current.params = p;
        }
        this.current.id = id;
        this.current.controller.js = SL.util.String.ucfirst(this.current.controller.js);
    }

};
