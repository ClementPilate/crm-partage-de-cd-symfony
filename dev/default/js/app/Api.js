SL.app.Api = class Api {

    static sendContact(post, callback, context) {
        let url = SL.main.path.getApiUrl() + "contact/send";
        SL.app.Api.post(url, post, callback, context);
    }

    static sendTerrain(post, callback, context) {
        let url = SL.main.path.getApiUrl() + "terrain/send";
        SL.app.Api.post(url, post, callback, context);
    }

    static post(url, post, callback, context) {
        $.ajax({
            types: 'POST',
            url: url,
            data: post,
            timeout: 0,
            context: context,
            success: callback,
            error: function (xhr, type) {
                //TODO alert('Ajax error!')
            }
        })
    }
};