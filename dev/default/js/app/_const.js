const DEVICE = {
    mobile: "mobile",
    tablet: "tablet",
    desktop: "desktop",
    default: "default"
};

const EVENT = {
    resize: "resize",
    click: "click",
    mousemove: "mousemove",
    mouseenter: "mouseenter",
    mouseleave: "mouseleave",
    touchmove: "touchmove",
    focus: "focus",
    blur: "blur",
    scroll: "scroll"
};

const TYPEOF = {
    nodelist: "nodelist",
    element: "element"
};

const SENS_LESS = -1;
const SENS_MORE = 1;