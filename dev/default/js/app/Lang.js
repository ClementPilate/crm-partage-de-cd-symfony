SL.app.Lang = class Lang {
    constructor() {
        this.list = SL.main.config.lang.list;
        this.default = SL.main.config.lang.default;
        this.setName();
    }

    setName() {
        let path = SL.main.path;
        this.name = SL.main.config.lang.default;
        if (this.list.length > 0) {
            if (path.getCurrent() != path.getBase()) {
                this.name = path.getCurrent().replace(path.getBase(), "");
                this.name = this.name.split("/");
                this.name = this.name[0];
            }
            if (this.name.indexOf("?") != -1) {
                this.name = this.name.split('?');
                this.name = this.name[0];
            }

            if(!this.checkLang()){
                this.name = SL.main.config.lang.default;
            }
        }
    }


    checkLang() {
        return this.list.indexOf(this.name) != -1;
    }
};