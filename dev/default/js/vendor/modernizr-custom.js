/*! modernizr 3.3.0 (Custom Build) | MIT *
 * http://modernizr.com/download/?-history-input-placeholder-setclasses !*/
!function (e, n, t) {
    function a(e, n) {
        return typeof e === n
    }

    function o() {
        var e, n, t, o, s, i, f;
        for (var u in l)if (l.hasOwnProperty(u)) {
            if (e = [], n = l[u], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))for (t = 0; t < n.options.aliases.length; t++)e.push(n.options.aliases[t].toLowerCase());
            for (o = a(n.fn, "function") ? n.fn() : n.fn, s = 0; s < e.length; s++)i = e[s], f = i.split("."), 1 === f.length ? Modernizr[f[0]] = o : (!Modernizr[f[0]] || Modernizr[f[0]] instanceof Boolean || (Modernizr[f[0]] = new Boolean(Modernizr[f[0]])), Modernizr[f[0]][f[1]] = o), r.push((o ? "" : "no-") + f.join("-"))
        }
    }

    function s(e) {
        var n = u.className, t = Modernizr._config.classPrefix || "";
        if (c && (n = n.baseVal), Modernizr._config.enableJSClass) {
            var a = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
            n = n.replace(a, "$1" + t + "js$2")
        }
        Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), c ? u.className.baseVal = n : u.className = n)
    }

    function i() {
        return "function" != typeof n.createElement ? n.createElement(arguments[0]) : c ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments)
    }

    var r = [], l = [], f = {
        _version: "3.3.0", _config: {classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0}, _q: [], on: function (e, n) {
            var t = this;
            setTimeout(function () {
                n(t[e])
            }, 0)
        }, addTest: function (e, n, t) {
            l.push({name: e, fn: n, options: t})
        }, addAsyncTest: function (e) {
            l.push({name: null, fn: e})
        }
    }, Modernizr = function () {
    };
    Modernizr.prototype = f, Modernizr = new Modernizr, Modernizr.addTest("history", function () {
        var n = navigator.userAgent;
        return -1 === n.indexOf("Android 2.") && -1 === n.indexOf("Android 4.0") || -1 === n.indexOf("Mobile Safari") || -1 !== n.indexOf("Chrome") || -1 !== n.indexOf("Windows Phone") ? e.history && "pushState" in e.history : !1
    });
    var u = n.documentElement, c = "svg" === u.nodeName.toLowerCase();
    Modernizr.addTest("placeholder", "placeholder" in i("input") && "placeholder" in i("textarea"));
    var d = i("input"), p = "autocomplete autofocus list placeholder max min multiple pattern required step".split(" "), m = {};
    Modernizr.input = function (n) {
        for (var t = 0, a = n.length; a > t; t++)m[n[t]] = !!(n[t] in d);
        return m.list && (m.list = !(!i("datalist") || !e.HTMLDataListElement)), m
    }(p), o(), s(r), delete f.addTest, delete f.addAsyncTest;
    for (var h = 0; h < Modernizr._q.length; h++)Modernizr._q[h]();
    e.Modernizr = Modernizr
}(window, document);