SL.manager.Event = class Event {

    /**
     * Add eventlistener
     * @param event
     * @param target
     * @param method
     * @param key
     * @param groupKey
     */
    static on(event, target, method, key, groupKey = "global") {

        let e =
        {
            id: key,
            target: target,
            event: event,
            groupId: groupKey,
            method: method
        };

        if (!SL.manager.Event.groups[groupKey]) {
            SL.manager.Event.groups[groupKey] = {};
        }
        if (!SL.manager.Event.groups[groupKey][key]) {
            SL.manager.Event.groups[groupKey][key] = {}
        }
        SL.manager.Event.groups[groupKey][key][event] = e;

        $(target).on(event, method);
    }

    /**
     * Remove event listener
     * @param event
     * @param key
     * @param groupKey
     */
    static off(event, key, groupKey = "global") {
        let e = SL.manager.Event.groups[groupKey][key][event];

        $(e.target).off(e.event, e.method);
        delete SL.manager.Event.groups[groupKey][key][event];
    }

    /**
     * Remove all listener for an event group
     * @param groupKey
     */
    static groupOff(groupKey) {
        let id = "";
        let event = "";
        let e = null;
        for (id in SL.manager.Event.groups[groupKey]) {

            for (event in SL.manager.Event.groups[groupKey][id]) {
                e = SL.manager.Event.groups[groupKey][id][event];
                $(e.target).off(e.event, e.method);
            }
        }
        delete SL.manager.Event.groups[groupKey];
    }
};
SL.manager.Event.groups = {};