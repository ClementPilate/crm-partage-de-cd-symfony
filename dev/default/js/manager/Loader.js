SL.manager.Loader = class Loader {
    constructor() {
        this.queue = new createjs.LoadQueue();
        this.queue.on("complete", this.onComplete, this);
        this.queue.on("progress", this.onProgress, this);
        this.loaded = 0;
    }

    loadContent(progress=null, complete=null) {
        let manifest = [];
        $("#content").find("img").each(function () {
            manifest.push({id: this.id, src: this.getAttribute("src"), types: "image"});
        });

        this.load(manifest,progress,complete);
    }

    loadManifest(manifest, progress, complete) {
        this.load(manifest,progress,complete);
    }

    load(manifest, progress, complete){
        this.loaded = 0;
        this.progress = progress?progress:this.progress;
        this.complete = complete?complete:this.complete;

        if(manifest.length === 0){
            this.loaded = 1;
            this.complete();
        }else{
            this.queue.loadManifest(manifest);
        }
    }

    onProgress(e) {
        this.loaded = e.loaded;
        this.progress();
    }

    progress() {
        console.log(this.loaded);
    }

    onComplete(e) {
        this.complete();
    }

    complete() {
        console.log("complete load");
    }

};