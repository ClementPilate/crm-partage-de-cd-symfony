SL.partial.Slider = class Slider {
    constructor(id, onChange = null, total = 1) {
        this.id = id;
        this.onChange = onChange;
        this.page = 0;
        this.sens = 0;
        this.prefix = "/";
        this.total = total;
        this.canSlide = true;
        this.html = {};
        this.setDom();
        this.addListeners();

        if (this.html.container) {
            this.total = this.html.container.getAttribute("data-total") ? parseInt(this.html.container.getAttribute("data-total")) : this.total;

            if (this.total <= 1) {
                this.html.container.style.display = "none";
            }
        }
    }

    setDom() {
        this.html.container = document.getElementById(this.id);
        this.html.label = document.getElementById(this.id + '-label');
        this.html.prev = document.getElementById(this.id + '-bt-prev');
        this.html.next = document.getElementById(this.id + '-bt-next');
    }

    addListeners() {
        $(this.html.prev).on(EVENT.click, this.onPagination.bind(this));
        $(this.html.next).on(EVENT.click, this.onPagination.bind(this));
    }

    removeListeners() {
        $(this.html.prev).off(EVENT.click, this.onPagination.bind(this));
        $(this.html.next).off(EVENT.click, this.onPagination.bind(this));
    }

    onPagination(e) {
        this.sens = (e.currentTarget === this.html.prev) ? SENS_LESS : SENS_MORE;
        this.set();
    }

    set() {
        if (!this.canSlide)return;
        this.canSlide = false;
        let page = this.page + this.sens;
        page = page >= this.total ? 0 : page;
        page = page < 0 ? (this.total - 1) : page;

        if (page != this.page) {
            this.page = page;
            if (this.html.label)this.html.label.innerHTML = (this.page + 1) + this.prefix + this.total;
            if (this.onChange)this.onChange(this.page, this.sens);
        }
    }

    clean() {
        this.removeListeners();

        for (let s in this) {
            this[s] = null;
        }
    }
};