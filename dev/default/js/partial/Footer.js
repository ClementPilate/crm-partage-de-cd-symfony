SL.partial.Footer = class Footer {
    constructor() {
        this.step = 0;
        this.setDom();
        this.addListeners();
    }

    setDom() {
        this.html = {};
        this.html.footer = document.querySelector(".footer");
        this.html.contact = document.querySelector(".footer-contact");
        this.html.num = document.querySelector(".footer-contact-stepper-num-current");
        this.html.mentions = document.getElementById("#footer-contact-credits-mentions-link");
    }

    addListeners() {
        $(this.html.mentions).on(EVENT.click, SL.main.onClickPageLink);
        $(".footer-contact-stepper-bt").on(EVENT.click, this.onClickNext);
        $(".footer-contact-form-btback").on(EVENT.click, this.onClickBack);
    }

    onClickNext(e) {
        if (!SL.main.page.footer.validStep())return;
        SL.main.page.footer.step++;
        SL.main.page.footer.onStepChange();
    }

    onClickBack(e) {
        SL.main.page.footer.step--;
        SL.main.page.footer.onStepChange();
    }

    onClickSend(e) {
        if (!SL.main.page.footer.validStep())return;

        let post = $(".footer-contact-form").serialize();
        SL.app.Api.sendContact(post, SL.main.page.footer.onSendContact, SL.main.page.footer);
    }

    onSendContact(rs) {
        let resultat = JSON.parse(rs);
        if (resultat.status === 1) {
            $(".footer-contact-form").hide();
            $(".footer-contact-confirm").css("display", "inline-block");
            this.html.footer.className = "footer";
            this.html.contact.className = "footer-contact";
        } else {
            console.log(rs);
        }
    }

    onStepChange() {
        $(".footer-contact-form-step").removeClass("footer-contact-form-step-selected");
        $("#footer-contact-form-step-" + this.step).addClass("footer-contact-form-step-selected");
        $(this.html.num).html((this.step + 1));

        if (this.step === 0) {
            $(".footer-contact-form-btback").hide();
        } else {
            $(".footer-contact-form-btback").show();
        }

        $(".footer-contact-stepper-bt").off(EVENT.click, this.onClickNext);
        $(".footer-contact-stepper-bt").off(EVENT.click, this.onClickSend);
        if (this.step === 2) {
            this.html.footer.className = "footer footer-step-2";
            this.html.contact.className = "footer-contact footer-contact-step-2";
            $(".footer-contact-stepper-bt-label").html("Envoyer");
            $(".footer-contact-stepper-bt").on(EVENT.click, this.onClickSend);

        } else {
            $(".footer-contact-stepper-bt-label").html("Suivant");
            $(".footer-contact-stepper-bt").on(EVENT.click, this.onClickNext);
            this.html.footer.className = "footer";
            this.html.contact.className = "footer-contact";
        }
    }

    validStep() {
        let check = true;
        let inputs = $("#footer-contact-form-step-" + this.step).find("input,textarea");

        let lg = inputs.length;
        for (let i = 0; i < lg; i++) {
            check = !SL.util.Validator.notempty(inputs[i]) ? false : check;
            if (inputs[i].name === "email") {
                check = !SL.util.Validator.email(inputs[i]) ? false : check;
            } else if (inputs[i].name === "zip") {
                check = !SL.util.Validator.zip(inputs[i]) ? false : check;
            }
        }

        return check;
    }

};