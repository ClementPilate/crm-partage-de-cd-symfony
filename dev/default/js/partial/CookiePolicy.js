SL.partial.CookiePolicy = class CookiePolicy {
    constructor() {
        this.html = document.getElementById('cookie');
        this.bt = document.getElementById('cookie-bt');
        this.name = SL.main.path.getBase() + "cookiepolicy";
        if (SL.util.Cookie.readCookie(this.name) != null) {
            this.close();
        }else{
            this.html.style.display = "block";
        }

        $(this.bt).on(EVENT.click,this.close.bind(this));
    }

    close() {
        SL.util.Cookie.createCookie(this.name, "", 395);
        this.html.parentNode.removeChild(this.html);
    };

};