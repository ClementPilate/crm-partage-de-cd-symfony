SL.partial.Loading = class Loading {
    constructor() {
        this.percent = 0;
        this.html = document.createElement("div");
        document.body.appendChild(this.html);
        this.html.style.fontSize = "36px";
        this.html.style.color = "#000";
        this.html.style.padding = "20px";
        this.html.style.overflow = "hidden";
        this.tl = new TimelineLite();
        this.tl.stop();
        this.tl.fromTo(this.html, 0.6,
            {position: "absolute", display: "block", left: 0, top: 0, width: "calc(100% - 40px)", height: 50, backgroundColor: "#fff"},
            {height: "calc(100% - 40px)", ease: Quart.easeOut}
        );
    }

    reset() {
        this.setPercent(0);
    }

    show(callback) {
        TweenLite.fromTo(this.html, 0.4, {width: 0, height: 0, display: "block", padding: "0 20px"}, {
            width: "calc(100% - 40px)", padding: 20, height: 50, ease: Quart.easeOut, onComplete: function () {
                if (callback)callback();
            }
        });
    }

    hide(callback = null) {
        TweenLite.killTweensOf(this.html);
        TweenLite.to(this.html, 0.8, {
            height: 0, padding: "0 20px", delay: 0.6, ease: Quart.easeOut, onComplete: function () {
                if (callback)callback();
            }
        });
    }

    setPercent(percent) {
        this.percent = percent;
        this.html.innerHTML = parseInt(SL.main.page.loader.loaded * 100).toString();
        this.tl.progress(percent);
    }

    clean() {
        this.tl.kill();
        document.removeChild(this.html);
        for (let s in this) {
            this[s] = null;
        }
    }
};