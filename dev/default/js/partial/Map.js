SL.partial.Map = class Map {
    constructor(id) {
        this.id = id;
        this.html = {};
        this.setDom();

        this.lat = parseFloat(this.html.map.getAttribute('data-lat'));
        this.lng = parseFloat(this.html.map.getAttribute('data-lng'));

        this.map = null;
        this.markers = [];
        this.infowindow = null;
        this.service = null;
        this.types = [];
        this.keywords = [];

        this.style = [
            {"visibility": "simplified"},
            {
                "featureType": "landscape",
                "stylers": [
                    {"color": "#e9e5dc"}
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {"color": "#ffffff"}
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                    {"color": "#ffffff"}
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {"color": "#8ef1dd"}
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {"color": "#000000"}
                ]
            }
        ];


        this.options = {
            zoom: 16,
            disableDefaultUI: true,
            zoomControl: true,
            panControl: true,
            scrollwheel: false,
            draggable: true,
            styles: this.style
        };

        this.addScript();

        this.icons = {
            "shop": {},
            "school": {},
            "spectacle": {}
        };

    }

    addScript() {
        if (window["google"] === undefined || window["google"].maps === undefined) {
            let script = document.createElement('script');
            script.types = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&callback=SL.main.page.controller.map.init';
            document.body.appendChild(script);
        } else {
            this.init();
        }
    }

    init() {
        this.addListeners();

        this.center = new google.maps.LatLng(this.lat, this.lng);

        this.map = new google.maps.Map(this.html.map, this.options);
        this.map.setCenter(this.center);

        this.infowindow = new google.maps.InfoWindow();

        this.service = new google.maps.places.PlacesService(this.map);
    }

    setDom() {
        this.html.map = document.getElementById(this.id);
    }

    addListeners() {
    }

    removeListeners() {
    }

    getIcon(place) {
        let rs = null;
        let icon = "default";
        let name = place.name.toLowerCase();
        let types = place.types;
        if (types.indexOf("post_office") != -1) {
            icon = "poste";
        } else if (types.indexOf("grocery_or_supermarket") != -1) {
            icon = "shop";
        } else if (types.indexOf("school") != -1) {
            icon = "school";
        } else if (types.indexOf("movie_theater") != -1) {
            icon = "cinema";
        } else if (types.indexOf("gym") != -1) {
            icon = "sport";
        } else if (types.indexOf("bar") != -1) {
            icon = "bar";
        } else if (types.indexOf("restaurant") != -1) {
            icon = "restaurant";
        } else if (types.indexOf("hospital") != -1) {
            icon = "hospital";
        } else if (types.indexOf("pharmacy") != -1) {
            icon = "pharmacie";
        } else if (types.indexOf("bus_station") != -1) {
            icon = "bus";
        } else if (types.indexOf("subway_station") != -1 || types.indexOf("transit_station") != -1) {
            icon = "metro";
        }

        if (name.indexOf("marché") != -1) {
            icon = "shop";
        } else if (name.indexOf("bibliothèque") != -1) {
            icon = "bibliotheque";
        } else if (name.indexOf("monument") != -1 || name.indexOf("historique") != -1 || name.indexOf("mémorial") != -1) {
            icon = "monument";
        } else if (name.indexOf("théâtre") != -1 || name.indexOf("theatre") != -1) {
            icon = "spectacle";
        } else if (name.indexOf("crèche") != -1 || name.indexOf("creche") != -1) {
            icon = "creche";
        } else if (name.indexOf("maternelle") != -1) {
            icon = "maternelle";
        } else if (name.indexOf("école primaire") != -1 || name.indexOf("ecole primaire") != -1) {
            icon = "school";
        } else if (name.indexOf("collège") != -1 || name.indexOf("college") != -1) {
            icon = "colleges";
        } else if (name.indexOf("lycee") != -1 || name.indexOf("lycee") != -1) {
            icon = "colleges";
        } else if (name.indexOf("enseignement") != -1 || name.indexOf("facul") != -1 || name.indexOf("universit") != -1) {
            icon = "colleges";
        } else if (name.indexOf("tramway") != -1) {
            icon = "metro";
        } else if (name.indexOf("vélo") != -1) {
            icon = "velo";
        }

        let anchor = new google.maps.Point(0, 61);
        let size = new google.maps.Size(62, 61);
        rs = {
            url: SL.main.path.getAssetsUrl() + "svg/map/" + icon + ".svg",
            anchor: anchor,
            scaledSize: size
        };
        return rs;
    }

    onSearchNearBy(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
                this.createMarker(results[i]);
            }
        }
    }

    createMarker(place) {

        let icon = this.getIcon(place);
        let placeLoc = place.geometry.location;
        let marker = new google.maps.Marker({
            map: this.map,
            position: place.geometry.location,
            icon: icon
        });

        google.maps.event.addListener(marker, 'click', function () {
            SL.main.page.controller.map.showInfoWindow(place, this);
        });

        this.markers.push(marker);
    }

    clearMarkers() {
        var i = 0;
        var lg = this.markers.length;
        for (i = 0; i < lg; i++) {
            this.markers[i].setMap(null);
        }
    }

    showInfoWindow(place, marker) {
        console.log(place);
        if (place.name && place.name != "" && place.name != " ") {
            this.infowindow.setContent('<div class="map-infowindow">' + place.name + '<br>' + place.vicinity + '</div>');
            this.infowindow.open(this.map, marker);
        }
    }


    /**
     * addPlaces
     * @param types array
     * @param keywords array
     */
    addPlaces(types, keywords) {
        this.clearMarkers();

        let i = 0;
        let lg = types.length;
        for (i = 0; i < lg; i++) {
            if (types[i] != '' && this.types.indexOf(types[i]) === -1) {
                this.types.push(types[i]);
            }
        }

        lg = keywords.length;
        for (i = 0; i < lg; i++) {
            if (keywords[i] != '' && this.keywords.indexOf(keywords[i]) === -1) {
                this.keywords.push(keywords[i]);
            }
        }
        this.searchNearBy();
    }

    removePlaces(types, keywords) {

        let at = null;
        let i = 0;
        let lg = types.length;
        for (i = 0; i < lg; i++) {
            at = this.types.indexOf(types[i]);
            if (at != -1) this.types.splice(at, 1);
        }

        lg = keywords.length;
        for (i = 0; i < lg; i++) {
            at = this.keywords.indexOf(keywords[i]);
            if (at != -1) this.keywords.splice(at, 1);
        }

        this.clearMarkers();
        this.searchNearBy();
    }

    searchNearBy() {

        let requests = [];
        let request = null;
        let i = 0;
        let lg = this.types.length;
        for (i = 0; i < lg; i++) {
            request = {
                location: this.center,
                radius: '2000',//meters,
                keyword: '',
                types: [this.types[i]]
            };
            requests.push(request);
            //this.service.nearbySearch(this.request, this.onSearchNearBy.bind(this));
        }

        lg = this.keywords.length;
        let keyword = "";
        for (i = 0; i < lg; i++) {
            request = {
                location: this.center,
                radius: '2000',//meters,
                keyword: this.keywords[i],
                types: []
            };
            requests.push(request);
            //this.service.nearbySearch(this.request, this.onSearchNearBy.bind(this));
        }
        lg = requests.length;
        for (i = 0; i < lg; i++) {
            setTimeout(function (request) {
                SL.main.page.controller.map.service.nearbySearch(request, SL.main.page.controller.map.onSearchNearBy.bind(SL.main.page.controller.map));
            }, i * 100, requests[i]);
        }
    }

    clean() {
        for (let s in this) {
            this[s] = null;
        }
    }
};