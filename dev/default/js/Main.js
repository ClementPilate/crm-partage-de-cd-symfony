SL.app.Main = class Main {

    constructor(baseurl) {

        console.log('%c Salty lighty. Version: Basic Banana ', 'color: #2b2b2b; display: block; line-height: 24px;');

        this.config = null;
        /**
         * @type Path
         */
        this.path = null; 
        /**
         * @type Lang
         */
        this.lang = null;
        /**
         * @type Route
         */
        this.route = null;
        /**
         * @type Page
         */
        this.page = null;
        var configUrl = baseurl + "config.json";
        SL.util.File.getFile(configUrl, this.onGetConfig, this);
    }

    onGetConfig(response) {
        this.config = response;
        this.set();
        this.addListeners();
        this.addTest();
    }

    set() {
        this.path = new SL.app.Path();
        this.lang = new SL.app.Lang();
        this.route = new SL.app.Route();
        this.page = new SL.app.Page();
        this.page.init();
    }

    addTest() {
        if (!Modernizr.input.placeholder) {
            SL.util.File.getFile(this.path.getDeviceUrl() + 'js/oldBrowser/placeholders.min.js');
        }
    }

    addListeners() {
        if (this.config.options.ajax) {
            History.Adapter.bind(window, 'statechange', this.onChangeHistory);
        }
    }

    setUrl(url) {
        if(url === SL.main.path.getCurrent())return;
        SL.main.path.setCurrent(url);
        var currentLang = SL.main.lang.name;
        SL.main.lang.setName();
        if (SL.main.lang.name != currentLang) {
            SL.main.route.setRoutes();
        }
        SL.main.route.setCurrent();
        SL.main.page.setCurrent();
    }

    onChangeHistory() {
        var state = History.getState();
        if (SL.main.path.getCurrent() === state.url)return;
        SL.main.setUrl(state.url);
    }

    onClickPageLink(e) {
        if (SL.main.config.options.ajax && e.preventDefault && Modernizr.history) {
            e.preventDefault();
            if (SL.main.page.header.mobileMenuOpened == true) SL.main.page.header.closeMobileMenu();
            SL.main.setUrl(this.href);

            let semantic = SL.main.page.local.semantic;

            History.pushState(null, SL.main.page.local.title, SL.main.path.getCurrent());

            //Analytics
            if (window["ga"]) {
                let pageview = '/' + semantic;
                ga('send', 'pageview', pageview);
            }
        } else {
            window.location.href = this.href;
        }
    }

};

callbackScriptLoaded();
