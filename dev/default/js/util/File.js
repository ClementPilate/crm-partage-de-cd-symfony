SL.util.File = class File {

    static getFile(url, callback, context, params = {}) {
        $.ajax({
            type: 'GET',
            url: url,
            data: params,
            timeout: 0,
            context: context,
            success: callback,
            error: function (xhr, type) {
                //TODO alert('Ajax error!')
            }
        });
    }

    static getPage(url, callback, context) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                ajax: true
            },
            timeout: 0,
            context: context,
            success: callback,
            error: function (xhr, type) {
                callback();
            }
        });
    }

};