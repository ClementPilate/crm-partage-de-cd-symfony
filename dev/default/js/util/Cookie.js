SL.util.Cookie = class Cookie {

    /**
     * createCookie
     * @param name
     * @param value
     * @param days
     */
    static createCookie(name, value, days) {
        let expires = "";
        if (days) {
            let date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        let cookie = name + '=' + value + expires + '; path=/';
        document.cookie = cookie;
    };

    /**
     * readCookie
     * @param name
     * @returns {*}
     */
    static readCookie(name) {
        let nameEQ = name;
        let ca = document.cookie.split(';');
        let c = 0;
        for (let i = 0; i < ca.length; i++) {
            c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    static eraseCookie(name) {
        createCookie(name, "", -1);
    };
};
