SL.util.Scroll = class Scroll {

    static easeTo(scrollX, scrollY, callback) {
        TweenLite.to({scrollX: window.pageXOffset, scrollY: window.pageYOffset}, 0.8, {
            scrollX: scrollX, scrollY: scrollY, onUpdate: function () {
                window.scrollTo(this.target.scrollX, this.target.scrollY);
            }, onComplete: callback
        });

    }

};