SL.util.Resize = class Resize {
    /**
     * @param elW
     * @param elH
     * @param contW
     * @param contH
     * @returns {{x: number, y: number, w: number, h: number}}
     */
    static letterBox(elW, elH, contW, contH) {
        var elRatio = elW / elH;
        var contRatio = contW / contH;
        var pos = {
            x: 0,
            y: 0,
            w: 0,
            h: 0
        };

        if (elRatio < contRatio) {
            pos.w = contW;
            pos.h = Math.round(pos.w / elRatio);
            pos.y = Math.round(-( pos.h - contH ) / 2);
        }
        else {
            pos.h = contH;
            pos.w = Math.round(pos.h * elRatio);
            pos.x = Math.round(-( pos.w - contW ) / 2);
        }

        return pos;
    }

};