SL.util.String = class String{

    static ucfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
};