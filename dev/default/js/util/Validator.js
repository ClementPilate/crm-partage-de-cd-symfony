SL.util.Validator = class Validator{

    static notempty(input) {
        let value = input.value;
        let $input = $(input);
        let check = value && value.trim().length > 0 && value != $input.attr("placeholder");

        $input.removeClass("error");
        if(!check)$input.addClass("error");
        return check;
    }

    static email(input) {
        let value = input.value;
        let $input = $(input);
        let regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let check = regex.test(value);

        $input.removeClass("error");
        if(!check)$input.addClass("error");
        return check;
    }

    static zip(input) {
        let value = input.value;
        let $input = $(input);
        let regex = /^[0-9]{5}$/;;
        let check = regex.test(value);

        $input.removeClass("error");
        if(!check)$input.addClass("error");
        return check;
    }

};