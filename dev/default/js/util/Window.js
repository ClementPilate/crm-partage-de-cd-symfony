/**
 *
 * @returns {Controller|*}
 */
function getController() {
    if (SL.main && SL.main.page) {
        return SL.main.page.controller;
    }
}