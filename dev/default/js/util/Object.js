SL.util.Object = class Object {

    static keyOf(object, value) {
        var find = false;
        for (let s in object) {
            if (object[s] === value) {
                find = s;
                break;
            }
        }
        return find;
    }

    static clone(target) {
        let clone = {};
        for (let s in target) {
            if (target.hasOwnProperty(s)) {
                clone[s] = target[s];
            }
        }
        return clone;
    }

};