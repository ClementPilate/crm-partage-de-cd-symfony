<?php
spl_autoload_register(array('Main', 'autoLoader'));
require_once 'vendor/autoload.php';

class Main
{
    protected static $instance;

    /**
     * @var \app\Lang
     */
    public $lang = null;
    /**
     * @var \app\Path
     */
    public $path = null;
    /**
     * @var \app\Route
     */
    public $route = null;
    /**
     * @var Mustache_Engine
     */
    public $moustache = null;
    /**
     * @var Page
     */
    public $page = null;
    public $config = null;
    public $header = "";
    public $footer = "";
    public $cookie = "";
    public $isold = false;
    public $oldbrowser = "";

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public static function autoLoader($className)
    {
        if (strstr($className, "\\")) {
            $className = str_replace("\\", "/", $className);
        }
        $file = __DIR__ . "/" . $className . ".php";
        if (file_exists($file)) {
            include_once $file;
        }
    }

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }


    public function init()
    {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
        $whoops->register();
        $this->moustache = new Mustache_Engine;
        $this->set();
        $this->setHeaderFooter();
    }

    protected function set()
    {
        //Set config
        $this->config = json_decode(file_get_contents("config.json"));
        if ($this->config->options->getDeviceInfo) {
            //Set device info
            $this->device = \app\Device::getInstance();
            $this->device->set($this->config);

            $this->isold = $this->device->isOld();
        }
        //Set path
        $this->path = \app\Path::getInstance();
        $this->path->set($this->config);
        //Set lang
        $this->lang = \app\Lang::getInstance();
        $this->lang->set($this->config);
        //Set routes
        $this->route = \app\Route::getInstance();
        $this->route->set($this->config);
        //Set page
        $this->page = new \app\Page($this->route->current->id, $this->route->current, $this->route->params);
    }

    protected function setHeaderFooter()
    {
        if (!isset($_POST["ajax"])) {

            if($this->isold){
                $tpl = file_get_contents($this->path->getTplPath() . "partial/oldbrowser.html");
                if ($tpl) {
                    $data = json_decode(file_get_contents($this->path->getLocalPath() . "partial/oldbrowser.json"));
                    $this->oldbrowser = $this->moustache->render($tpl, $data);
                }
            }else{
                $gzip = $this->config->env->name === "dist" ? ".gz" : "";

                //HEADER
                $tpl = file_get_contents($this->path->getTplPath() . "partial/header.html");
                $data = json_decode(file_get_contents($this->path->getLocalPath() . "partial/header.json"));
                $data->urls = $this->route->urls;
                $data->local = $this->route->current->local;
                $data->lang = $this->lang->name;
                $data->canUseGa = $this->canUseGa();
                $data->recaptcha = $this->config->options->recaptcha ? true : null;
                $data->class = $this->route->current->id === "home" ? "" : "header-reduced";
                $data->gzip = $gzip;
                $data->getPage = function ($data, Mustache_LambdaHelper $helper) {
                    $page = $helper->render($data);
                    return \app\Path::getInstance()->getPageUrl($page);
                };

                //SVG
                $data->svg = file_get_contents($this->path->getTplPath() . "partial/svg.html");
                $this->header = $this->moustache->render($tpl, $data);

                //FOOTER
                $tpl = file_get_contents($this->path->getTplPath() . "partial/footer.html");
                $data = json_decode(file_get_contents($this->path->getLocalPath() . "partial/footer.json"));
                $data->canUseGa = $this->canUseGa();
                $data->recaptcha = $this->config->options->recaptcha ? true : null;
                $data->urls = $this->route->urls;
                $data->urls->base = $this->path->url->base;
                $data->gzip = $gzip;
                $data->getPage = function ($data, Mustache_LambdaHelper $helper) {
                    $page = $helper->render($data);
                    return \app\Path::getInstance()->getPageUrl($page);
                };
                $this->footer = $this->moustache->render($tpl, $data);

                //COOKIE
                if ($this->lang->name === "fr") {
                    $tpl = file_get_contents($this->path->getTplPath() . "partial/cookie.html");
                    if ($tpl) {
                        $data = json_decode(file_get_contents($this->path->getLocalPath() . "partial/cookie.json"));
                        $this->cookie = $this->moustache->render($tpl, $data);
                    }
                }
            }
        }
    }

    public function canUseGa(){
        return (string)$this->config->env->name === "dist" ? true : null;
    }

    public function render()
    {
        if($this->isold){
            echo $this->oldbrowser;
        }else{
            echo $this->header
                . $this->page->render
                . $this->footer
                . $this->cookie;
        }

    }

}