<?php

namespace app;

class Device
{
    protected static $instance;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public $client = "";
    public $os = "";
    public $device = "";
    public $brand = "";
    public $model = "";

    public function set($config)
    {
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";
        $dd = new \DeviceDetector\DeviceDetector($user_agent);

        $dd->parse();

        if (!$dd->isBot()) {
            $this->client = (object)$dd->getClient(); // holds information about browser, feed reader, media player, ...
            $this->os = $dd->getOs();
            $this->brand = $dd->getBrand();
            $this->model = $dd->getModel();
        }

        $config->devices->client = $this->client;
        $config->devices->os = $this->os;
        $config->devices->brand = $this->brand;
        $config->devices->model = $this->model;

        file_put_contents("config.json",json_encode($config));
    }

    public function isOld(){
        $check = false;
        //Detect old browser
        if($this->client->type === "browser"){
            if($this->client->short_name === "IE" && (integer)$this->client->version < 9) {
                $check = true;
            }else if($this->client->short_name === "CH" && (integer)$this->client->version < 36){
                $check = true;
            }else if($this->client->short_name === "FF" && (integer)$this->client->version < 36){
                $check = true;
            }else if($this->client->short_name === "SF" && (integer)$this->client->version < 6){
                $check = true;
            }else if($this->client->short_name === "OP" && (integer)$this->client->version < 12){
                $check = true;
            }
        }

        return $check;
    }
}