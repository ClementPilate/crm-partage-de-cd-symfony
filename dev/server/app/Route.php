<?php
namespace app;

class Route
{
    protected static $instance;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public $semantics = null;
    public $urls = null;
    public $routes = null;
    public $current = null;
    public $params = null;

    public function set($config)
    {
        $this->routes = $config->routes;
        $this->setRoutes($config);
        $this->setCurrentPage();
    }

    protected function setRoutes($config)
    {
        $lang = Lang::getInstance();
        $path = Path::getInstance();

        $this->semantics = array();
        $this->routes = new \stdClass();
        $this->urls = new \stdClass();
        foreach ($config->routes as $k => $route) {
            $this->semantics[$k] = $route->local->{$lang->name}->semantic;

            $route->local = $route->local->{$lang->name};
            $this->routes->{$k} = $route;
            //$this->urls->{$k} = $path->getPageUrl($k);
        }

        $this->urls->current = new \stdClass();
        $this->urls->current->link = $path->getCurrentUrl();
        $this->urls->current->lang = $lang->name;
        $this->urls->current->alternates = [];//$path->getCurrentUrl();
        //Set alternates routes
        foreach ($lang->list as $l) {
            if($l === $lang->name)continue;
            $url = new \stdClass();
            $url->link = $this->urls->current->link;
            $url->link = str_replace($lang->name,$l,$url->link);
            $url->lang = $l;
            $this->urls->current->alternates[] = $url;
        }


        $this->urls->assets = $path->getAssetsUrl();
        $this->urls->css = $path->getDeviceUrl() . "css/";
        $this->urls->js = $path->getDeviceUrl() . "js/";

        //print_r($this->urls);exit;
    }

    protected function setCurrentPage()
    {
        $lang = Lang::getInstance();
        $path = Path::getInstance();

        //Check lang
        if (!$lang->checkLang()) header('Location: ' . $path->getBaseUrl($lang->default));

        $find = false;
        $tocheck = $path->url->fullCurrent;
        $tocheck = str_replace($path->url->base, "", $tocheck);
        $tocheck = str_replace($lang->name . "/", "", $tocheck);

        if (strstr($tocheck, "?")) {
            $tocheck = explode("?", $tocheck);
            $tocheck = $tocheck[0];
        }

        $params = explode("/", $tocheck);
        $tocheck = $params[0];
        unset($params[0]);
        $params = array_values($params);

        $route = $this->routes->page404;
        $id = array_search($tocheck, $this->semantics);
        if (in_array($tocheck, $this->semantics) && $id) {
            header("Status: 200 OK", false, 200);
            $tocheck = $tocheck === "" ? "home" : $tocheck;
            $route = $this->routes->{$id};
        } else {
            header("Status: 404 NOT FOUND", false, 404);
            $id = "page404";
        }
        $route->id = $id;
        $this->current = $route;
        $this->params = $params;
    }

}