<?php

namespace app;


class Lang
{

    protected static $instance;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public $name = "";
    public $list = array();
    public $default = array();
    public $local = null;

    public function set($config)
    {
        $this->list = $config->lang->list;
        $this->default = $config->lang->default;
        $this->setName($config);
    }

    protected function setName($config)
    {
        $path = Path::getInstance();
        if (count($this->list) === 0) {
            $this->name = $config->lang->default;
        } else {
            if ($path->url->fullCurrent != $path->url->base) {
                $this->name = str_replace($path->url->base, "", $path->url->fullCurrent);
                $this->name = explode("/", $this->name);
                $this->name = $this->name[0];
            } else if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $user_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
                $this->name = $user_lang;
            }
            //print_r($this->name);exit;
            if (strstr($this->name, "?")) {
                $this->name = explode("?", $this->name);
                $this->name = $this->name[0];
            }

            if(!$this->checkLang()){
                $this->name = $config->lang->default;
            }
            //=print_r(var_dump($this->name));exit;
        }
    }


    public function checkLang()
    {
        return in_array($this->name, $this->list);
    }

}