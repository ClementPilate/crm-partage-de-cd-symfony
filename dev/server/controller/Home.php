<?php

namespace controller;

class Home extends AbstractController
{
    public function setDynamicData($local, $params = null)
    {
        $local = parent::setDynamicData($local, $params);

        //Call api here and return $local ( if needed )

        return $local;
    }

}