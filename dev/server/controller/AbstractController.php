<?php
namespace controller;

class AbstractController
{

    protected $params = null;

    public function __construct()
    {
    }

    /**
     * Return data from apis to add in the page or anything that isn't in the local
     * @param \stdClass $params
     */
    public function setDynamicData($local, $params = null)
    {
        $d = new \stdClass();
        $d->params = new \stdClass();

        foreach ($params as $k => $param) {
            $d->params->{$k} = $param;
        }

        $this->params = $d->params;

        $local->data = $this->params;

        return $local;
    }
}